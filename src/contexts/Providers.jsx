import ConfigContext from './ConfigContext'
import LanguageContext from './LanguageContext'
import ServerContext from './ServerContext'

export function ContextsProviders ({ children }) {
  return (
    <ConfigContext.Provider>
      <LanguageContext.Provider>
        <ServerContext.Provider>{children}</ServerContext.Provider>
      </LanguageContext.Provider>
    </ConfigContext.Provider>
  )
}
