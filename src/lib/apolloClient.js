import fetch from 'isomorphic-unfetch'
import cookies from 'next-cookies'
import { HttpLink } from 'apollo-link-http'
import { ApolloClient } from 'apollo-client'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { URL_API } from 'utils/urls'
import { makeid } from 'utils/functions'
import { encode } from 'js-base64'

export default function createApolloClient (initialState, ctx) {
  const token = ctx
    ? cookies(ctx).jwt
    : typeof window !== 'undefined'
    ? localStorage.getItem('jwt') && localStorage.getItem('jwt') != 'undefined'
      ? JSON.parse(localStorage.getItem('jwt'))
      : ''
    : ''

  const init = makeid(10) + '_' + makeid(12)
  const token_lifetime = 60 * 60 * 12
  const expiration = Math.round(Date.now() / 1000 + token_lifetime)
  const base_64 = encode(expiration.toString())
  const send = init + '_' + makeid(5) + base_64

  return new ApolloClient({
    ssrMode: Boolean(ctx),
    link: new HttpLink({
      uri: `${URL_API}/gql`,
      credentials: 'same-origin',
      fetch,
      headers: {
        authorization: `Bear ${token}`,
        'x-access-jwt-token': token,
        'x-access-platform': send,
        referer: 'https://doramasflix.in/',
        platform: 'doramasflix'
      }
    }),
    cache: new InMemoryCache().restore(initialState)
  })
}
