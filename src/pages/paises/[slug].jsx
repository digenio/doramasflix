import { Letters } from 'components/Letters'

import { Page } from 'layouts/Page'
import { Space } from 'components/Space'
import { CountryList } from 'containers/CountryList'
import { withApollo } from '../../lib/apollo'
import { LayoutPage } from 'layouts/LayoutPage'
import { getDeviceType } from 'utils/functions'

function Country (props) {
  return (
    <Page>
      <LayoutPage
        header={<Space height='8rem' />}
        extraHeader={<Letters />}
        widgets={['views', 'friends', 'follow']}
      >
        <CountryList {...props} />
      </LayoutPage>
    </Page>
  )
}

Country.getInitialProps = ({ req, query }) => {
  const device = getDeviceType(req)
  return {
    ...device,
    ...query
  }
}

export default withApollo({ ssr: false })(Country)
