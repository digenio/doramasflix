import { Page } from 'layouts/Page'
import { DoramaDetail } from 'containers/DoramaDetail'
import { withApollo } from '../../lib/apollo'

import { getDeviceType } from 'utils/functions'

function Dorama (props) {
  return (
    <Page>
      <DoramaDetail {...props} />
    </Page>
  )
}

Dorama.getInitialProps = ({ req, query }) => {
  const device = getDeviceType(req)
  return {
    ...device,
    ...query
  }
}

export default withApollo({ ssr: true })(Dorama)
