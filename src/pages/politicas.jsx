import { Page } from 'layouts/Page'
import { Head } from 'components/Head'
import { Space } from 'components/Space'
import { Privacity } from 'components/Terms/Privacity'
import { withApollo } from '../lib/apollo'
import { LayoutPage } from 'layouts/LayoutPage'

function CookiesPage () {
  return (
    <Page>
      <LayoutPage
        header={<Space height='8rem' />}
        widgets={['views', 'friends', 'follow']}
      >
        <Head title={`Politicas de Privacidad ► Doramasflix`} />
        <Privacity />
      </LayoutPage>
    </Page>
  )
}

export default withApollo({ ssr: false })(CookiesPage)
