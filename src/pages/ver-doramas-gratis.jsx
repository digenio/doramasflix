import { Head } from 'components/Head'
import { Page } from 'layouts/Page'
import { Letters } from 'components/Letters'
import { LayoutPage } from 'layouts/LayoutPage'
import { withApollo } from 'lib/apollo'
import { DoramasFreeList } from 'containers/DoramasFreeList'
import { getDeviceType } from 'utils/functions'
import { DoramasCarousel } from 'containers/DoramasCarrousel'
import { URL } from 'utils/urls'

function DoramasFree ({ deviceType }) {
  return (
    <Page>
      <LayoutPage
        header={<DoramasCarousel deviceType={deviceType} />}
        extraHeader={<Letters />}
        widgets={['views', 'follow', 'friends']}
      >
        <Head
          title={`Ver Doramas Gratis Online Sub Español HD 📺 Doramasflix`}
          description={`Ver doramas online gratis en Doramasflix ⚡️ En Doramasflix mirar dramas online en castellano, Latino, Español y Subtitulado HD nunca fue tan fácil.`}
          url={`${URL}/ver-doramas-gratis`}
        />
        <DoramasFreeList deviceType={deviceType} />
      </LayoutPage>
    </Page>
  )
}

DoramasFree.getInitialProps = ({ req }) => getDeviceType(req)

export default withApollo({ ssr: false })(DoramasFree)
