import { Page } from 'layouts/Page'
import { Head } from 'components/Head'
import { Space } from 'components/Space'
import { Letters } from 'components/Letters'
import { LetterList } from 'containers/LetterList'
import { withApollo } from '../../lib/apollo'
import { LayoutPage } from 'layouts/LayoutPage'
import { getDeviceType } from 'utils/functions'

function Letter (props) {
  return (
    <Page>
      <LayoutPage
        header={<Space height='8rem' />}
        extraHeader={<Letters {...props} />}
        widgets={['views', 'friends', 'follow']}
      >
        <Head title={`Doramas y peliculas ► Doramasflix`} />
        <LetterList {...props} />
      </LayoutPage>
    </Page>
  )
}

Letter.getInitialProps = ({ req, query }) => {
  const device = getDeviceType(req)
  return {
    ...device,
    ...query
  }
}

export default withApollo({ ssr: true })(Letter)
