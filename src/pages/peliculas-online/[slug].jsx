import { Page } from 'layouts/Page'
import { withApollo } from '../../lib/apollo'
import { MovieDetail } from 'containers/MovieDetail'
import { getDeviceType } from 'utils/functions'

function Movie (props) {
  return (
    <Page>
      <MovieDetail {...props} />
    </Page>
  )
}

Movie.getInitialProps = ({ req, query }) => {
  const device = getDeviceType(req)
  return {
    ...device,
    ...query
  }
}

export default withApollo({ ssr: true })(Movie)
