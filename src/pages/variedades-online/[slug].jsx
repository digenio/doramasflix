import { Page } from 'layouts/Page'
import { ShowDetail } from 'containers/ShowDetail'
import { withApollo } from '../../lib/apollo'

import { getDeviceType } from 'utils/functions'

function Shows (props) {
  return (
    <Page>
      <ShowDetail {...props} />
    </Page>
  )
}

Shows.getInitialProps = ({ req, query }) => {
  const device = getDeviceType(req)
  return {
    ...device,
    ...query
  }
}

export default withApollo({ ssr: true })(Shows)
