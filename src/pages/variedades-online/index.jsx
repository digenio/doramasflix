import { Head } from 'components/Head'
import { Page } from 'layouts/Page'
import { Letters } from 'components/Letters'
import { LayoutPage } from 'layouts/LayoutPage'
import { withApollo } from '../../lib/apollo'
import { DoramasList } from 'containers/DoramasList'
import { getDeviceType } from 'utils/functions'
import { DoramasCarousel } from 'containers/DoramasCarrousel'
import { URL } from 'utils/urls'

function Doramas ({ deviceType }) {
  return (
    <Page>
      <LayoutPage
        header={<DoramasCarousel deviceType={deviceType} tvShows />}
        extraHeader={<Letters />}
        widgets={['views', 'friends', 'follow']}
      >
        <Head
          title={`Variedades ⋆ Ver KShows Online - Mira TV Shows Corea Sub Español Online 🥇 Doramasflix`}
          description={`Ver los mejores ⭐️ KShows ⭐️ Online Gratis en Sub Español, Latino en HD Gratis. Ver variedades nunca fue tan fácil en Doramasflix.`}
          url={`${URL}/variedades-online`}
        />
        <DoramasList deviceType={deviceType} tvShows />
      </LayoutPage>
    </Page>
  )
}

Doramas.getInitialProps = ({ req }) => getDeviceType(req)

export default withApollo({ ssr: false })(Doramas)
