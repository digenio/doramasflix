import { Page } from 'layouts/Page'
import { EpisodeDetail } from 'containers/EpisodeDetail'
import { withApollo } from '../../lib/apollo'
import { getDeviceType } from 'utils/functions'

function Capitulo (props) {
  return (
    <Page>
      <EpisodeDetail {...props} />
    </Page>
  )
}

Capitulo.getInitialProps = ({ req, query }) => {
  const device = getDeviceType(req)
  return {
    ...device,
    ...query
  }
}

export default withApollo({ ssr: true })(Capitulo)
