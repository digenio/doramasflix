import { Page } from "layouts/Page";
import { Space } from "components/Space";
import { Letters } from "components/Letters";
import { withApollo } from "../lib/apollo";
import { LayoutPage } from "layouts/LayoutPage";

function Store() {
  return (
    <Page>
      <LayoutPage
        header={<Space height="8rem" />}
        extraHeader={<Letters />}
        widgets={["views", "friends", "follow"]}
      ></LayoutPage>
    </Page>
  );
}

Store.getInitialProps = ({ res }) => {
  res.writeHead(301, {
    location:
      "https://play.google.com/store/apps/details?id=com.asiapp.doramasgo",
  });
  res.end();
};

export default withApollo({ ssr: false })(Store);
