import { Page } from 'layouts/Page'
import { Head } from 'components/Head'
import { Space } from 'components/Space'
import { withApollo } from '../lib/apollo'
import { LayoutPage } from 'layouts/LayoutPage'
import { CalendarDoramas } from 'containers/CalendarDoramas'
import { getDeviceType } from 'utils/functions'
import { URL } from 'utils/urls'

function Calendar ({ deviceType }) {
  return (
    <Page>
      <LayoutPage
        header={<Space height={deviceType === 'laptop' ? '8rem' : '5rem'} />}
        widgets={[]}
      >
        <Head
          title={`Programación Estrenos Doramas (2021) Online Sub Español - Doramasflix`}
          description={`Ver el día de los ⭐️ Estrenos Doramas ⭐️ online subtitulados al español. En Doramasflix, nunca fue tan fácil ver la programación de Doramas en el 2021.`}
          url={`${URL}/calendario`}
        />

        <CalendarDoramas deviceType={deviceType} />
      </LayoutPage>
    </Page>
  )
}

Calendar.getInitialProps = ({ req }) => getDeviceType(req)

export default withApollo({ ssr: false })(Calendar)
