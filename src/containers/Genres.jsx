import React from 'react'
import dynamic from 'next/dynamic'

import { useNearScreen } from 'hooks/useNearScreen'

const GenreList = dynamic(() => import('components/WidgetList/GenreList'))

export function Genres () {
  const { isNearScreen, fromRef } = useNearScreen({
    distance: '100px'
  })

  return (
    <div style={{ minHeight: 150 }} ref={fromRef}>
      {isNearScreen && <GenreList />}
    </div>
  )
}
