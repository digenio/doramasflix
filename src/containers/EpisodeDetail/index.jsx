import { useEffect } from "react";
import { Head } from "components/Head";

import { useQuery, useLazyQuery } from "@apollo/react-hooks";

import { Space } from "components/Space";
import { Player } from "components/Player";
import { Loading } from "components/Loading";
import { ShareEpisode } from "components/ShareEpisode";

import { EPISODE_DETAIL, EPISODE_DETAIL_EXTRA, EPISODE_LINKS } from "gql/episode";
import { HeaderEpisode } from "./Header";
import { Comments } from "containers/Details/Comments";
import { URL, URL_IMAGE_EPISODE_2X } from "utils/urls";
import { NotFound } from "components/NotFound";
import { SEASON_DETAIL_ID } from "gql/season";

export function EpisodeDetail({ slug }) {
  const { data: { detailEpisode } = {}, loading } = useQuery(EPISODE_DETAIL, {
    variables: { slug },
  });

  const [getLinks, { data: { getEpisodeLinks } = {} }] = useLazyQuery(EPISODE_LINKS);

  const [detailEpisodeExtra, { data }] = useLazyQuery(EPISODE_DETAIL_EXTRA);

  const [getSeason, { data: { detailSeasonById } = {} }] = useLazyQuery(SEASON_DETAIL_ID);

  useEffect(() => {
    if (detailEpisode) {
      getLinks({
        variables: {
          id: detailEpisode._id,
          app: "com.asiapp.doramasgo",
        },
      });
      getSeason({
        variables: {
          _id: detailEpisode.season_id,
        },
      });
      detailEpisodeExtra({
        variables: {
          episode_id: detailEpisode._id,
          slug: detailEpisode.serie_slug,
        },
      });
    }
  }, [detailEpisode]);

  if (loading) {
    return (
      <>
        <Space height="6rem" />
        <Loading />
      </>
    );
  }

  if (!detailEpisode) {
    return (
      <>
        <Space height="8rem" />
        <NotFound title="No se encontro el episodio deseado deseado." />
      </>
    );
  }

  const { nextEpisode, prevEpisode, detailDorama } = data || {};

  const {
    _id,
    languages,
    serie_name,
    serie_slug,
    serie_poster,
    still_path,
    season_poster,
    season_number,
    episode_number,
    serie_backdrop_path,
    air_date,
    sources,
    countDownDate,
  } = detailEpisode;

  const { links_online = [] } = getEpisodeLinks || {};

  return (
    <>
      <Head
        title={`Ver ${serie_name} ${
          season_number > 1 ? ` ${season_number} ` : ""
        }episodio ${episode_number} online sub español HD ► Doramasflix`}
        description={`Ver ${serie_name}${
          season_number > 1 ? ` temporada ${season_number}` : ""
        } episodio ${episode_number} Online ⭐️  Dorama ${serie_name} ${season_number}X${episode_number} sub español, latino en HD Gratis ⭐️  ${serie_name} ep ${episode_number} gratis.`}
        image={URL_IMAGE_EPISODE_2X + (still_path || serie_poster || season_poster)}
        url={`${URL}/episodios/${slug}`}
      />
      <Player
        langs={languages}
        links={links_online}
        backdrop_path={serie_backdrop_path}
        title={`${serie_name} ${season_number > 1 ? `${season_number} ` : ""}episodio ${episode_number}`}
        season_number={season_number}
        slug={serie_slug}
        next={nextEpisode}
        prev={prevEpisode}
        episode={detailEpisode}
        air_date={air_date}
        sources={sources}
        _id={_id}
        dorama={detailDorama}
        countDownDate={countDownDate}
        season={detailSeasonById}
      />
      <ShareEpisode
        url={`${URL}/episodios/${slug}`}
        name={`${serie_name} ${season_number > 1 ? `${season_number}` : ""} episodio ${episode_number}`}
      />
      <HeaderEpisode detailEpisode={detailEpisode}>
        <Comments
          name={`${serie_name} ${season_number > 1 ? `${season_number}` : ""} episodio ${episode_number}`}
          _id={_id}
        />
        <Space height="3rem" />
      </HeaderEpisode>
    </>
  );
}
