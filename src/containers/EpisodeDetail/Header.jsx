import { LayoutPage } from 'layouts/LayoutPage'
import { MetaEpisode } from 'components/Meta/Episode'

export function HeaderEpisode ({ detailEpisode, children }) {
  const { serie_name, season_number, episode_number } = detailEpisode

  return (
    <LayoutPage widgets={['follow', 'views', 'friends']}>
      <MetaEpisode
        name={serie_name}
        season={season_number}
        number={episode_number}
      />
      {children}
    </LayoutPage>
  )
}
