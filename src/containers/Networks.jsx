import { useQuery } from '@apollo/react-hooks'

import { NETWORK_LIST } from 'gql/network'
import { NetworksList } from 'components/NetwoksList'

export function Networks () {
  const { data: { listNetworks = [] } = {} } = useQuery(NETWORK_LIST, {
    variables: { limit: 0, sort: 'NUMBER_OF_DORAMAS_DESC', skip: 0 }
  })

  if (listNetworks.length === 0) {
    return null
  }

  return <NetworksList list={listNetworks} />
}
