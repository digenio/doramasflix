import { useQuery } from '@apollo/react-hooks'

import { Loading } from 'components/Loading'
import { ListEpisodes } from 'components/List/epidisodes'
import { LIST_EPISODES } from 'gql/episode'

export default function EpisodeList ({ serie_id, season_number = 1 }) {
  const { data: { listEpisodes = [] } = {}, loading } = useQuery(
    LIST_EPISODES,
    {
      variables: { serie_id, season_number },
      ssr: false
    }
  )

  if (loading) {
    return <Loading />
  }

  return <ListEpisodes list={listEpisodes} />
}
