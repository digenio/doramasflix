import { useQuery } from '@apollo/react-hooks'

import { Loading } from 'components/Loading'
import { ListSeasons } from 'components/List/season'
import { LIST_SEASON } from 'gql/season'

export default function SeasonList ({ serie_id }) {
  const { data: { listSeasons = [] } = {}, loading } = useQuery(LIST_SEASON, {
    variables: { serie_id },
    ssr: false
  })

  if (loading) {
    return <Loading />
  }

  return <ListSeasons list={listSeasons} />
}
