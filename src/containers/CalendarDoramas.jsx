import { IoMdCalendar } from 'react-icons/io'
import { useQuery } from '@apollo/react-hooks'

import { TitleSection } from 'components/TitleSection'
import { LIST_DORAMAS_CALENDAR } from 'gql/dorama'
import { Calendar } from 'components/Calendar'
import { Loading } from 'components/Loading'
import { monthNames } from '../utils/constans'

export function CalendarDoramas ({ deviceType }) {
  const { data: { listDoramas = [] } = {}, loading } = useQuery(
    LIST_DORAMAS_CALENDAR
  )

  const month = new Date().getUTCMonth()

  return (
    <>
      <TitleSection
        title={`Programación del mes de ${monthNames[month]}`}
        Icon={IoMdCalendar}
      />

      <div>
        ¡Annyeonghaseyo! 🇰🇷 🙌 Si eres fan de los <strong>doramas</strong>, no
        te puedes perder el siguiente listado que se actualiza diariamente, en
        donde se detallan los mejores{' '}
        <strong>estrenos de doramas online</strong> en el 2021. Todo cambio de
        los <strong>K-Dramas en 2021</strong> se visualizará en este cuadro de{' '}
        <strong>programación de doramas</strong>. Recuerda que en Doramasflix,
        puedes disfrutar de los doramas en Sub Español, Latino en HD Gratis y
        Completas.
      </div>

      {loading ? (
        <Loading />
      ) : (
        <Calendar list={listDoramas} deviceType={deviceType} />
      )}
    </>
  )
}
