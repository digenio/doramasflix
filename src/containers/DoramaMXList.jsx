import { MdMovie } from 'react-icons/md'
import { useState } from 'react'
import { useQuery } from '@apollo/react-hooks'

import { List } from 'components/List'
import { Pagination } from 'components/Pagination'
import { TitleSection } from 'components/TitleSection'
import { PAGINATION_DORAMAS } from 'gql/dorama'
import { MetaContainer, MetaStrong } from 'components/Meta/styles'

export function DoramasMXList ({ deviceType }) {
  const perPage =
    deviceType === 'mobile' ? 12 : deviceType === 'tablet' ? 20 : 32
  const [page, changePage] = useState(1)

  const variables = {
    sort: 'CREATEDAT_DESC',
    page,
    perPage,
    filter: { bylanguages: ['38'] }
  }

  const { data: { paginationDorama = {} } = {}, loading } = useQuery(
    PAGINATION_DORAMAS,
    {
      variables
    }
  )

  const { items = [], count } = paginationDorama

  return (
    <>
      <TitleSection
        title='Doramas en español Latino'
        Icon={MdMovie}
      ></TitleSection>
      <MetaContainer>
        Ver <MetaStrong>doramas en latino</MetaStrong> online gratis en 720p HD
        y 1080p Full HD. Recuerda que en Doramasflix, puedes disfrutar de los{' '}
        <MetaStrong>doramas en el idioma latino</MetaStrong> en Sub Español,
        Latino en HD Gratis y Completas.
      </MetaContainer>
      <List items={items} loading={loading} type='doramas' />
      <Pagination
        deviceType={deviceType}
        count={count}
        perPage={perPage}
        onChange={changePage}
        page={page}
      />
    </>
  )
}
