import { useQuery } from '@apollo/react-hooks'

import { Carousel } from 'components/Carousel'
import { LIST_DORAMAS } from 'gql/dorama'
import { CarrouselLoader } from 'components/Loader/Carousel'
import { shuffleArray } from 'utils/functions'

export function DoramasCarousel ({ deviceType, tvShows = false }) {
  const { data: { listDoramas = [] } = {}, loading } = useQuery(LIST_DORAMAS, {
    variables: {
      sort: 'POPULARITY_DESC',
      filter: { in_carrousel: true, isTVShow: tvShows }
    }
  })

  if (loading) {
    return <CarrouselLoader />
  }

  return (
    <Carousel
      list={shuffleArray(listDoramas).slice(0, 8)}
      deviceType={deviceType}
    />
  )
}
