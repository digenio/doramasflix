import React from 'react'
import dynamic from 'next/dynamic'

import { useNearScreen } from 'hooks/useNearScreen'

const Doramas = dynamic(() => import('./Doramas'))

export function HomeDoramas ({ deviceType }) {
  const { isNearScreen, fromRef } = useNearScreen({
    distance: '200px'
  })

  return (
    <div style={{ minHeight: 210 }} ref={fromRef}>
      {isNearScreen && <Doramas deviceType={deviceType} />}
    </div>
  )
}
