import React from 'react'
import dynamic from 'next/dynamic'

import { useNearScreen } from 'hooks/useNearScreen'

const Episodes = dynamic(() => import('./Episodes'))

export function PremiereEpisodes ({ deviceType }) {
  const { isNearScreen, fromRef } = useNearScreen({
    distance: '100px'
  })

  return (
    <div style={{ minHeight: 150 }} ref={fromRef}>
      {isNearScreen && <Episodes deviceType={deviceType} />}
    </div>
  )
}
