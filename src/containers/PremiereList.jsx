import { MdMovie } from 'react-icons/md'
import { useQuery } from '@apollo/react-hooks'

import { copy } from 'utils/functions'
import { List } from 'components/List'
import { TitleSection } from 'components/TitleSection'
import { PAGINATION_DORAMAS } from 'gql/dorama'
import { MetaContainer, MetaStrong } from 'components/Meta/styles'

export function PremiereList () {
  const variables = {
    sort: 'CREATEDAT_DESC',
    filter: { premiere: true }
  }

  const { data: { paginationDorama = {} } = {}, loading } = useQuery(
    PAGINATION_DORAMAS,
    {
      variables: copy(variables)
    }
  )

  const { items = [], count } = paginationDorama

  return (
    <>
      <TitleSection title='Doramas en Emisión' Icon={MdMovie} />

      <MetaContainer>
        Ver <MetaStrong>estrenos doramas</MetaStrong> online gratis en 720p HD y
        1080p Full HD. Recuerda que en Doramasflix, puedes disfrutar de los{' '}
        <MetaStrong>estrenosdoramas</MetaStrong> en Sub Español, Latino en HD
        Gratis y Completas.
      </MetaContainer>
      <List items={items} loading={loading} type='doramas' />
    </>
  )
}
