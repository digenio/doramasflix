import { useQuery } from "@apollo/react-hooks";

import { Item } from "components/Carousel/Item";
import { Head } from "components/Head";
import { Cast } from "components/Cast";
import { Space } from "components/Space";
import { NotFound } from "components/NotFound";
import { Comments } from "containers/Details/Comments";
import { Container } from "styles/base";
import { MetaMovie } from "components/Meta/Movie";
import { LayoutPage } from "layouts/LayoutPage";
import { PlayerMovie } from "components/Player/movie";
import { DETAIL_MOVIES, MOVIE_LINKS } from "gql/movie";
import { CarrouselLoader } from "components/Loader/Carousel";
import { URL_IMAGE_POSTER, URL } from "utils/urls";

export function MovieDetail({ slug }) {
  const { data: { detailMovie } = {}, loading } = useQuery(DETAIL_MOVIES, {
    variables: { slug },
  });

  const { data: { getMovieLinks } = {}, loading: ld2 } = useQuery(MOVIE_LINKS, {
    variables: { slug, app: "com.asiapp.doramasgo" },
  });

  if (!detailMovie) {
    return (
      <>
        {loading || ld2 ? (
          <CarrouselLoader />
        ) : (
          <>
            <Space height="8rem" />
            <NotFound title="No se encontro la pelicula deseada." />
          </>
        )}
      </>
    );
  }

  const { _id, name, poster_path, cast, release_date, name_es, poster } = detailMovie;

  const links_online = getMovieLinks ? getMovieLinks.links_online : [];

  return (
    <>
      <Head
        title={`Ver Pelicula ${name} (${
          release_date && release_date.split("-")[0]
        }) Online Sub Español HD ► Doramasflix`}
        description={`Ver ${name} Online ⭐️ Pelicula ${name_es || name} en Sub Español, Latino en HD Gratis`}
        image={poster || URL_IMAGE_POSTER + poster_path}
        url={`${URL}/peliculas-online/${slug}`}
      />
      <Item {...detailMovie} full title />
      <Container>
        <PlayerMovie links={links_online} movie={detailMovie} />
      </Container>
      <LayoutPage header={<Space height="4rem" />} widgets={["friends", "follow"]}>
        <MetaMovie name={name} />
        <Cast cast={cast} name={name} />
        <Comments _id={_id} />
        <Space height="3rem" />
      </LayoutPage>
    </>
  );
}
