import React from 'react'
import dynamic from 'next/dynamic'

import { useNearScreen } from 'hooks/useNearScreen'

const Movies = dynamic(() => import('./Movies'))

export function HomeMovies ({ deviceType }) {
  const { isNearScreen, fromRef } = useNearScreen({
    distance: '200px'
  })

  return (
    <div style={{ minHeight: 210 }} ref={fromRef}>
      {isNearScreen && <Movies deviceType={deviceType} />}
    </div>
  )
}
