import React from 'react'
import dynamic from 'next/dynamic'

import { useNearScreen } from 'hooks/useNearScreen'

const Shows = dynamic(() => import('./Shows'))

export function HomeShows ({ deviceType }) {
  const { isNearScreen, fromRef } = useNearScreen({
    distance: '200px'
  })

  return (
    <div style={{ minHeight: 210 }} ref={fromRef}>
      {isNearScreen && <Shows deviceType={deviceType} />}
    </div>
  )
}
