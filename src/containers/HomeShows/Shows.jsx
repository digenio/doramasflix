import { useState } from 'react'
import { useQuery } from '@apollo/react-hooks'

import { HomeList } from 'components/HomeList'
import { LIST_DORAMAS } from 'gql/dorama'

export default function Shows ({ deviceType }) {
  const limit = deviceType === 'mobile' ? 15 : deviceType === 'tablet' ? 20 : 24
  const [sort, changeSort] = useState('CREATEDAT_DESC')
  const { data: { listDoramas = [] } = {}, loading } = useQuery(LIST_DORAMAS, {
    variables: {
      sort,
      limit,
      skip: 0,
      filter: {
        isPublishWeb: true,
        isTVShow: true
      }
    },
    ssr: false
  })

  return (
    <HomeList
      list={listDoramas}
      loading={loading}
      title='Variedades'
      sort={sort}
      changeSort={changeSort}
      url='/variedades-online'
      type='variedades'
    />
  )
}
