import { useQuery } from '@apollo/react-hooks'

import { LIST } from 'gql/both'
import { Space } from 'components/Space'
import { Carousel } from 'components/Carousel'
import { mixArray, shuffleArray } from 'utils/functions'
import { CarrouselLoader } from 'components/Loader/Carousel'

export function HomeCarousel ({ deviceType }) {
  const {
    data: { listDoramas = [], listMovies = [] } = {},
    loading
  } = useQuery(LIST)

  if (loading) {
    return <CarrouselLoader />
  }

  const list = mixArray(
    shuffleArray(listDoramas).slice(0, 7),
    shuffleArray(listMovies).slice(0, 3),
    10
  )

  if (list.length === 0) {
    return <Space height='8rem' />
  }

  return <Carousel list={list} deviceType={deviceType} />
}
