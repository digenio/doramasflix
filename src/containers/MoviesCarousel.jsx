import { useQuery } from '@apollo/react-hooks'

import { Space } from 'components/Space'
import { Loading } from 'components/Loading'
import { Carousel } from 'components/Carousel'
import { LIST_MOVIES } from 'gql/movie'
import { shuffleArray } from 'utils/functions'

export function MoviesCarousel ({ deviceType }) {
  const { data: { listMovies = [] } = {}, loading } = useQuery(LIST_MOVIES, {
    variables: { sort: 'POPULARITY_DESC', filter: { in_carrousel: true } }
  })

  if (listMovies.length === 0) {
    return (
      <>
        <Space height='8rem' />
        {loading && <Loading />}
      </>
    )
  }

  return (
    <Carousel
      list={shuffleArray(listMovies).slice(0, 8)}
      deviceType={deviceType}
    />
  )
}
