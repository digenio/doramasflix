export const countries = [
  {
    name: 'Corea del Sur',
    flag: '🇰🇷',
    slug: 'corea',
    type: 'Coreanos',
    letter: 'K'
  },
  { name: 'China', flag: '🇨🇳', slug: 'china', type: 'Chinos', letter: 'C' },
  { name: 'Japon', flag: '🇯🇵', slug: 'japon', type: 'Japoneses', letter: 'J' },
  {
    name: 'Taiwan',
    flag: '🇹🇼',
    slug: 'taiwan',
    type: 'Taiwaneses',
    letter: 'T'
  },
  {
    name: 'Tailandia',
    flag: '🇹🇭',
    slug: 'tailandia',
    type: 'Tailandeses',
    letter: 'T'
  }
]

export const languages = {
  13109: 'corea',
  13111: 'china',
  13110: 'japon',
  13112: 'tailandia',
  38: 'mexico'
}

export const languagesList = [
  { id: '38', name: 'Latino' },
  { id: '13109', name: 'Coreano Sub.' },
  { id: '13111', name: 'Chino Sub.' },
  { id: '13110', name: 'Japones Sub.' },
  { id: '13112', name: 'Tailandes Sub.' }
]

export const qualities = {
  123: 'Full HD',
  124: 'HD'
}

export const servers = {
  34: 'Fembed',
  395: 'Uqload',
  3890: 'Supervideo',
  2671: 'Mystream',
  3889: 'Mixdrop',
  6705: 'Jetload',
  7286: 'Dood',
  8309: 'Streamtape',
  8310: 'Gounlimited',
  14463: 'UpStream',
  14707: 'Flixplayer',
  1113: 'Ok',
  1114: 'Mega',
  1111: 'FB',
  1230: 'Voe',
  1231: 'Viwol',
  1235: 'Fembed',
  1233: 'Uqload',
  1234: 'mp4Upload',
  5555: 'Server',
  1236: 'Prime',
  1237: 'ED HD',
  1238: 'ED Mobile',
  1239: 'StreamSB',
  8888: 'FlixPlayer',
  1240: 'Voe Movil',
  1241: 'Mixdrop Movil',
  1245: 'DMP4',
  1000: 'Streamp4'
}

export const langsName = {
  38: 'Latino',
  13109: 'Sub Español',
  13110: 'Sub Español',
  13111: 'Sub Español',
  13112: 'Sub Español'
}

export const langsNameAb = {
  38: 'La',
  13109: 'Ko',
  13110: 'Jp',
  13111: 'Ch',
  13112: 'Ta'
}

export const webs = [
  {
    name: 'DoramasGO',
    url: 'https://doramasflix.co/doramasgo'
  },
  { name: 'Doramas Online', url: 'https://doramasflix.co' },
  //{ name: 'SeriesFlix', url: 'https://seriesflix.la/' },
  { name: 'PeliculasFlix', url: 'https://peliculasflix.co/' },
  //{ name: 'SeriesGO', url: 'https://seriesgo.co/' },
  { name: 'DoramasMp4', url: 'https://doramasmp4.io' },
  { name: 'DoramasVip', url: 'https://doramasflix.io/doramas' },
  { name: 'EstrenosDoramas', url: 'https://doramasflix.in/estrenos' },
  { name: 'Doramas Gratis', url: 'https://doramasmp4.io/doramas' }
]

export const letters = 'abcdefghijklmnopqrstuvwxyz'.split('')

export const collection = ['Viendo', 'Por ver', 'Vistos', 'Por continuar']

export const monthNames = [
  'Enero',
  'Febrero',
  'Marzo',
  'Abril',
  'Mayo',
  'Junio',
  'Julio',
  'Agosto',
  'Septiembre',
  'Octubre',
  'Novienbre',
  'Diciembre'
]

export const serversAvalibles = [
  '1000',
  '1113',
  '1239',
  '1237',
  '1245',
  '2671',
  '1238',
  '1240',
  '1241',
  '14463',
  '1233',
  '7286',
  '1235',
  '3889',
  '1234',
  '8309',
  '34',
  '1230',
  '395',
  '8888'
]

export const days = [
  'Domingo',
  'Lunes',
  'Martes',
  'Miércoles',
  'Jueves',
  'Viernes',
  'Sábado'
]
