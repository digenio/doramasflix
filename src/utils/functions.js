import MobileDetect from "mobile-detect";

export function shuffle(array1, array2) {
  array1 = array1;
  array2 = array2;

  const array3 = array1.concat(array2);
  const array4 = Array.from(Array(array3.length).keys());
  const array5 = [];

  for (let i = array4.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * i);
    const temp = array4[i];
    array4[i] = array4[j];
    array4[j] = temp;
  }

  array4.forEach((i) => {
    array5.push(copy(array3[i]));
  });

  return array5;
}

export function makeid(length) {
  var result = "";
  var characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

export function mixArray(array1, array2, limit) {
  const array = Array.from(Array(limit).keys());

  let idx = 0;
  let idx2 = 0;
  const list = [];
  array.forEach((item) => {
    if (item % 2 === 0) {
      if (array1[idx2]) {
        list.push(array1[idx2]);
        idx2++;
      } else {
        list.push(array2[idx]);
        idx++;
      }
    } else {
      if (array2[idx]) {
        list.push(array2[idx]);
        idx++;
      } else {
        list.push(array1[idx2]);
        idx2++;
      }
    }
  });

  const newList = list.filter((item) => item);

  return newList;
}

export function copy(obj) {
  return JSON.parse(JSON.stringify(obj));
}

export function getDeviceType(req) {
  let userAgent;
  let deviceType;
  if (req) {
    userAgent = req.headers["user-agent"];
  } else {
    userAgent = navigator.userAgent;
  }
  const md = new MobileDetect(userAgent);
  if (md.tablet()) {
    deviceType = "tablet";
  } else if (md.mobile()) {
    deviceType = "mobile";
  } else {
    deviceType = "desktop";
  }
  return { deviceType };
}

export const createArray = (n) => Array.from(Array(n).keys());

export const filterCast = (array) => array.filter((item) => item?.slug);

export const firstDayMonth = () => {
  let d = new Date();
  let month = d.getMonth() + 1;
  if (month / 9 <= 1) month = "0" + month;
  let year = d.getFullYear();
  let dateStr = year + "-" + month + "-" + "01";
  return dateStr;
};

export function formatDate(date) {
  var d = new Date(date),
    month = "" + (d.getMonth() + 1),
    day = "" + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = "0" + month;
  if (day.length < 2) day = "0" + day;

  return [year, month, day].join("-");
}

export function shuffleArray(array) {
  return array.sort(() => Math.random() - 0.5);
}

export function getDateString(date) {
  if (typeof date === "string") {
    if (date.includes("T")) {
      date = new Date(
        date
          .replace("T00:00:00.000Z", "T10:00:00.000Z")
          .replace("T05:00:00.000Z", "T10:00:00.000Z")
      );
    } else {
      date = new Date(date + "T10:00:00.000Z");
    }
  }

  var dayOfWeek = [
    "Domingo",
    "Lunes",
    "Martes",
    "Miércoles",
    "Jueves",
    "Viernes",
    "Sábado",
  ];

  const monthName = [
    "Enero",
    "Febrero",
    "Marzo",
    "Abril",
    "Mayo",
    "Junio",
    "Julio",
    "Agosto",
    "Septiembre",
    "Octubre",
    "Noviembre",
    "Diciembre",
  ];

  if (!date) {
    return "";
  }

  return (
    dayOfWeek[date.getDay()] +
    " " +
    date.getDate() +
    " de " +
    monthName[date.getMonth()] +
    " del " +
    date.getFullYear()
  );
}

export function isBeforeDateMargin2(date) {
  if (!date) return true;
  const date1 = new Date(
    date
      .replace("T00:00:00.000Z", "T10:00:00.000Z")
      .replace("T05:00:00.000Z", "T10:00:00.000Z")
  );
  const date2 = new Date();

  date1.setDate(date1.getDate() + 3);

  return date1 > date2;
}
