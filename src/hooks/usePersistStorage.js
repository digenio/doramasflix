import { useEffect, useState } from 'react'

export function usePersistStorage (key, initialValue) {
  const [value, changeVal] = useState(initialValue)
  const [restored, changeRestored] = useState(false)

  useEffect(() => {
    try {
      const value = localStorage.getItem(key)
      if (value) {
        changeVal(JSON.parse(value))
      } else {
        changeVal(initialValue)
      }
    } catch (error) {
      changeVal(initialValue)
    }
    changeRestored(true)
  }, [])

  const changeValue = newVal => {
    changeVal(newVal)
    localStorage.setItem(key, JSON.stringify(newVal))
  }

  return [value, changeValue, restored]
}
