import gql from 'graphql-tag'

export const TREND_SERIES = gql`
  {
    trendsDoramas(limit: 15) {
      _id
      name
      name_es
      slug
      poster_path
    }
  }
`
