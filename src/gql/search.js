import gql from 'graphql-tag'

export const SEARCH = gql`
  query searchAll($input: String!) {
    searchDorama(input: $input, limit: 5) {
      _id
      slug
      name
      name_es
      poster_path
      poster
    }
    searchMovie(input: $input, limit: 5) {
      _id
      name
      name_es
      slug
      poster_path
      poster
    }
  }
`
