import gql from 'graphql-tag'

export const LIST_SERVERS = gql`
  query listServers($_ids: [MongoID!]!) {
    listServeresByIds(_ids: $_ids) {
      _id
      name
      app
      code_flix
    }
  }
`
