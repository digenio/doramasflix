import gql from 'graphql-tag'

export const SEARCH = gql`
  query searchAll($input: String!) {
    searchDorama(input: $input, limit: 10) {
      _id
      slug
      name
      name_es
    }
    searchMovie(input: $input, limit: 10) {
      _id
      name
      name_es
      slug
    }
  }
`

export const LIST = gql`
  {
    listDoramas(filter: { in_carrousel: true }) {
      _id
      name
      name_es
      slug
      cast
      names
      overview
      created_by
      popularity
      poster_path
      backdrop_path
      first_air_date
      episode_run_time
      isTVShow
      poster
      backdrop
      genres {
        name
        slug
      }
      networks {
        name
        slug
      }
    }
    listMovies(filter: { in_carrousel: true }) {
      _id
      name
      name_es
      slug
      cast
      names
      overview
      popularity
      poster_path
      backdrop_path
      release_date
      runtime
      genres {
        name
        slug
      }
      networks {
        name
        slug
      }
    }
  }
`

export const LIST_POPULARS = gql`
  query list($limit: Int, $skip: Int) {
    listDoramas(limit: $limit, skip: $skip, sort: POPULARITY_DESC) {
      name
      name_es
      slug
      poster_path
      poster
    }
    listMovies(limit: $limit, skip: $skip, sort: POPULARITY_DESC) {
      name
      name_es
      slug
      poster_path
      poster
    }
  }
`

export const LIST_VIEWS = gql`
  {
    listDoramas(limit: 6, sort: CREATEDAT_ASC) {
      name
      name_es
      slug
      poster_path
      poster
    }
    listMovies(limit: 6, sort: CREATEDAT_ASC) {
      name
      name_es
      slug
      poster_path
      poster
    }
  }
`
