import gql from 'graphql-tag'

export const LIST_LABELS = gql`
  {
    listLabels {
      name
      _id
    }
  }
`

export const LABEL_SERIES_LIST = gql`
  query listDoramas($labelId: MongoID!, $limit: Int!) {
    listDoramas(filter: { labelId: $labelId }, limit: $limit) {
      _id
      name
      name_es
      poster_path
    }
  }
`

export const LANG_SERIES_LIST = gql`
  query listDoramas($filter: FilterFindManyDoramaInput!) {
    listDoramas(filter: $filter) {
      _id
      name
      name_es
      poster_path
    }
  }
`
