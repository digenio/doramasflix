import gql from 'graphql-tag'

export const GET_CONFIG = gql`
  query detailConfig {
    detailConfig(filter: { platform: "doramasgo" }) {
      name
      isMantein
      servers {
        name
        ref
      }
      languages {
        name
        ref
      }
      countries {
        name
        ref
      }
    }
  }
`
