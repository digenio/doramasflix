import gql from 'graphql-tag'

export const LIST_LANGUAGES = gql`
  query listLanguages($_ids: [MongoID!]!) {
    listLanguageesByIds(_ids: $_ids) {
      _id
      name
      flag
      code
      code_flix
    }
  }
`
