import gql from "graphql-tag";

export const LIST_MOVIES = gql`
  query listMovies($limit: Int, $skip: Int, $sort: SortFindManyMovieInput, $filter: FilterFindManyMovieInput) {
    listMovies(limit: $limit, skip: $skip, sort: $sort, filter: $filter) {
      _id
      name
      name_es
      slug
      cast
      names
      overview
      languages
      popularity
      poster_path
      vote_average
      backdrop_path
      release_date
      runtime
      poster
      backdrop
      genres {
        name
      }
      networks {
        name
      }
    }
  }
`;

export const LIST_MOVIES_BY_IDS = gql`
  query listMovies($_ids: [MongoID!]!) {
    listMoviesByIds(_ids: $_ids, limit: 200) {
      _id
      name
      name_es
      slug
      cast
      names
      overview
      languages
      popularity
      poster_path
      vote_average
      backdrop_path
      release_date
      runtime
      poster
      backdrop
      genres {
        name
      }
      networks {
        name
      }
    }
  }
`;

export const LIST_MOVIES_SLUG = gql`
  {
    listMovies(filter: { movie_type: "dorama" }, sort: UPDATEDAT_ASC) {
      slug
      cast
      updatedAt
    }
  }
`;

export const PAGINATION_MOVIES = gql`
  query listMovies($page: Int, $perPage: Int, $sort: SortFindManyMovieInput, $filter: FilterFindManyMovieInput) {
    paginationMovie(page: $page, perPage: $perPage, sort: $sort, filter: $filter) {
      count
      pageInfo {
        currentPage
        hasNextPage
        hasPreviousPage
      }
      items {
        _id
        name
        name_es
        slug
        cast
        names
        overview
        languages
        popularity
        poster_path
        vote_average
        backdrop_path
        release_date
        runtime
        poster
        backdrop
        genres {
          name
        }
        networks {
          name
        }
      }
    }
  }
`;

export const SEARCH_MOVIE = gql`
  query searchMovie($input: String!) {
    searchMovie(input: $input, limit: 10) {
      _id
      poster_path
      poster
      name
      names
      slug
      name_es
    }
  }
`;

export const DETAIL_MOVIES = gql`
  query detailMovie($slug: String!) {
    detailMovie(filter: { slug: $slug }) {
      _id
      name
      name_es
      slug
      cast
      names
      overview
      languages
      popularity
      poster_path
      vote_average
      backdrop_path
      release_date
      runtime
      poster
      backdrop
      genres {
        name
        slug
      }
      networks {
        name
        slug
      }
    }
  }
`;

export const MOVIE_LINKS = gql`
  query GetMovieLinks($id: MongoID, $slug: String, $app: String, $iosapp: String, $externalLink: String) {
    getMovieLinks(id: $id, slug: $slug, app: $app, iosapp: $iosapp, externalLink: $externalLink) {
      links_online
    }
  }
`;
