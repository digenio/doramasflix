import gql from 'graphql-tag'

export const GENRE_LIST = gql`
  query listGenres($limit: Int, $skip: Int, $sort: SortFindManyGenreInput) {
    listGenres(
      limit: $limit
      skip: $skip
      sort: $sort
      filter: { type_series: ["dorama", "movie"] }
    ) {
      _id
      name
      images
      slug
      number_of_doramas
    }
  }
`

export const LIST_GENRE_SLUG = gql`
  {
    listGenres(
      sort: UPDATEDAT_ASC
      filter: { type_series: ["dorama", "movie"] }
    ) {
      slug
      updatedAt
    }
  }
`
