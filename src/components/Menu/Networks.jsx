import dynamic from 'next/dynamic'
import { useNearScreen } from 'hooks/useNearScreen'
import { ItemIcon, ItemMenu, ItemIconUp, ItemIconDown, ListDiv } from './styles'
import { MdSwitchVideo, MdArrowDropDown, MdArrowDropUp } from 'react-icons/md'

const NetworkList = dynamic(() => import('./NetworkList'))

export function Networks () {
  const { isNearScreen, fromRef } = useNearScreen({
    distance: '0px'
  })
  return (
    <>
      <ItemMenu>
        <ItemIcon>
          <MdSwitchVideo size={15} />
        </ItemIcon>
        Productoras
        <ItemIconDown>
          <MdArrowDropDown size={25} />
        </ItemIconDown>
      </ItemMenu>
      <ItemIconUp>
        <MdArrowDropUp size={50} />
      </ItemIconUp>
      <ListDiv ref={fromRef}>{isNearScreen && <NetworkList />}</ListDiv>
    </>
  )
}
