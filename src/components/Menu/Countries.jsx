import dynamic from 'next/dynamic'
import { useNearScreen } from 'hooks/useNearScreen'
import { MdLanguage, MdArrowDropDown, MdArrowDropUp } from 'react-icons/md'
import { ItemIcon, ItemMenu, ItemIconUp, ItemIconDown, ListDiv } from './styles'

const CountryList = dynamic(() => import('./CountryList'))

export function Countries () {
  const { isNearScreen, fromRef } = useNearScreen({
    distance: '0px'
  })
  return (
    <>
      <ItemMenu>
        <ItemIcon>
          <MdLanguage size={15} />
        </ItemIcon>
        Paises
        <ItemIconDown>
          <MdArrowDropDown size={25} />
        </ItemIconDown>
      </ItemMenu>
      <ItemIconUp>
        <MdArrowDropUp size={50} />
      </ItemIconUp>
      <ListDiv ref={fromRef}>{isNearScreen && <CountryList />}</ListDiv>
    </>
  )
}
