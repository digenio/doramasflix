import Link from "next/link";
import { useState } from "react";
import { MdMenu, MdClose, MdLocalMovies, MdVideoLibrary } from "react-icons/md";
import { FcAndroidOs } from "react-icons/fc";
import { IoMdCalendar } from "react-icons/io";
import { FiMonitor } from "react-icons/fi";

import { Genres } from "./Genres";
import { Networks } from "./Networks";
import { Countries } from "./Countries";
import { Nav, List, Item, ItemIcon, ItemMenu, MenuButton } from "./styles";

export function Menu() {
  const [visibe, changeVisible] = useState(false);

  const toggleVisible = () => changeVisible(!visibe);

  return (
    <>
      <MenuButton visible={visibe} onClick={toggleVisible}>
        {visibe ? <MdClose size={27} /> : <MdMenu size={27} />}
      </MenuButton>
      <Nav visible={visibe}>
        <List>
          <Item>
            <Link href="/doramas-online">
              <a className="ItemMenu">
                <ItemIcon>
                  <MdVideoLibrary size={15} />
                </ItemIcon>
                Doramas
              </a>
            </Link>
          </Item>
          <Item>
            <Link href="/peliculas-online">
              <a className="ItemMenu">
                <ItemIcon>
                  <MdLocalMovies size={15} />
                </ItemIcon>
                Peliculas
              </a>
            </Link>
          </Item>
          <Item>
            <Link href="/variedades-online">
              <a className="ItemMenu">
                <ItemIcon>
                  <FiMonitor size={15} />
                </ItemIcon>
                Variedades
              </a>
            </Link>
          </Item>
          <Item>
            <Genres />
          </Item>
          <Item>
            <Networks />
          </Item>
          <Item>
            <Countries />
          </Item>
          <Item>
            <Link href="/calendario">
              <a className="ItemMenu">
                <ItemIcon>
                  <IoMdCalendar size={15} />
                </ItemIcon>
                Calendario
              </a>
            </Link>
          </Item>
          <Item>
            <ItemMenu href="https://play.google.com/store/apps/details?id=com.asiapp.doramasgo">
              <ItemIcon>
                <FcAndroidOs size={18} />
              </ItemIcon>
              App Movil
            </ItemMenu>
          </Item>
        </List>
      </Nav>
    </>
  );
}
