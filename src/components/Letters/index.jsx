import Link from 'next/link'

import { letters } from 'utils/constans'
import { List, Item, ItemLink } from './styles'

export function Letters ({ letter }) {
  return (
    <List>
      {letters.map((l, i) => (
        <Item key={i} active={l === letter}>
          <Link href='/letras/[letter]' as={`/letras/${l}`}>
            <a>{l}</a>
          </Link>
        </Item>
      ))}
    </List>
  )
}
