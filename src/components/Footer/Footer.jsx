import { MdInsertDriveFile } from 'react-icons/md'
import {
  FooterContainer,
  FooterNav,
  FooterRows,
  FooterSidebar,
  FooterSidebarItem,
  FooterSidebarSpan,
  FooterSidebarLink,
  FooterSidebarList,
  FooterSidebarIcon,
  FooterTitle,
  FooterTop,
  FooterBottom,
  FooterBottomText
} from './styles'
import { Container } from 'styles/base'
import Link from 'next/link'

export function Footer () {
  return (
    <FooterContainer>
      <FooterTop>
        <Container>
          <FooterRows>
            <FooterNav>
              <FooterSidebar>
                <FooterTitle>Páginas de apoyo</FooterTitle>
                <FooterSidebarList>
                  <FooterSidebarItem>
                    <Link href='/dcma'>
                      <a>
                        <FooterSidebarIcon>
                          <MdInsertDriveFile size={20} />
                        </FooterSidebarIcon>
                        <FooterSidebarSpan>DCMA</FooterSidebarSpan>
                      </a>
                    </Link>
                  </FooterSidebarItem>
                  <FooterSidebarItem>
                    <Link href='/politicas'>
                      <a>
                        <FooterSidebarIcon>
                          <MdInsertDriveFile size={20} />
                        </FooterSidebarIcon>
                        <FooterSidebarSpan>
                          Políticas de Cookies
                        </FooterSidebarSpan>
                      </a>
                    </Link>
                  </FooterSidebarItem>
                  <FooterSidebarItem>
                    <Link href='/faq'>
                      <a>
                        <FooterSidebarIcon>
                          <MdInsertDriveFile size={20} />
                        </FooterSidebarIcon>
                        <FooterSidebarSpan>
                          Preguntas Frecuentes
                        </FooterSidebarSpan>
                      </a>
                    </Link>
                  </FooterSidebarItem>
                  <FooterSidebarItem>
                    <Link href='/terminos'>
                      <a>
                        <FooterSidebarIcon>
                          <MdInsertDriveFile size={20} />
                        </FooterSidebarIcon>
                        <FooterSidebarSpan>
                          Términos y condiciones
                        </FooterSidebarSpan>
                      </a>
                    </Link>
                  </FooterSidebarItem>
                </FooterSidebarList>
              </FooterSidebar>
            </FooterNav>
          </FooterRows>
        </Container>
      </FooterTop>
      <FooterBottom>
        <Container>
          <FooterBottomText>
            Este sitio web no almacena ningún archivo en su servidor. Todo el
            contenido es proporcionado por terceras partes las cuales no están
            afiliadas a esta web.
          </FooterBottomText>
        </Container>
      </FooterBottom>
    </FooterContainer>
  )
}
