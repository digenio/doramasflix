import { FooterAlert, FooterAlertP, FooterAlertContainer } from './styles'

export function Alert () {
  return (
    <FooterAlertContainer>
      <FooterAlert></FooterAlert>
    </FooterAlertContainer>
  )
}
