import { DayCalendar, NameDay, TitleDay, DaysSeries } from './styles'
import { MdNavigateNext, MdNavigateBefore } from 'react-icons/md'
import theme from 'styles/theme'
import { DayItem } from './DayItem'

export function Day ({ day, list, active, next, back }) {
  const listDay = list.filter(
    it =>
      it?.schedule?.days &&
      it?.schedule?.days.find(d => d.toLowerCase() === day.toLowerCase())
  )

  return (
    <DayCalendar>
      <TitleDay active={active}>
        {back && (
          <MdNavigateBefore
            onClick={back}
            size={30}
            color={theme.colors.white}
          />
        )}
        <NameDay>{day}</NameDay>
        {next && (
          <MdNavigateNext onClick={next} size={30} color={theme.colors.white} />
        )}
      </TitleDay>
      <DaysSeries>
        {listDay.map((serie, idx) => (
          <DayItem key={idx} {...serie} />
        ))}
      </DaysSeries>
    </DayCalendar>
  )
}
