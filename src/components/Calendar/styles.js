import styled from 'styled-components'
import { addOpacityToColor } from 'styles/utils'

export const CarrouselCalendar = styled.div`
  background-color: ${({ theme }) => theme.colors.bg};
  display: flex;
  overflow: scroll;
`

export const DayCalendar = styled.div`
  width: 90%;
  margin: 5px 15px;

  @media ${({ theme }) => theme.device.mobileL} {
    width: 300px;
  }
`

export const TitleDay = styled.div`
  background-color: ${({ theme, active }) =>
    active ? theme.colors.primary : theme.colors.tinyBlack};
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 10px 10px;
  @media ${({ theme }) => theme.device.mobileL} {
    width: 300px;
  }
`

export const DaysSeries = styled.div`
  background-color: ${({ theme }) => theme.colors.gray};
  padding: 10px 10px;
  //min-height: calc(100vh - 200px);
  height: calc(100vh - 200px);
  overflow: scroll;
  @media ${({ theme }) => theme.device.mobileL} {
    width: 300px;
  }
`

export const NameDay = styled.span`
  color: ${({ theme }) => theme.colors.white};
  font-size: 1.2rem;
  font-weight: bold;
  text-align: center;
  width: 100%;

  @media ${({ theme }) => theme.device.mobileL} {
    font-size: 1rem;
  }
`

export const DayItemCalendar = styled.a`
  height: 130px;
  cursor: pointer;
  display: flex;
  align-items: center;
`

export const DayPoster = styled.div`
  height: 130px;
  width: 100px;
`

export const DayInfo = styled.div`
  height: 130px;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
`

export const DayInfoName = styled.p`
  color: ${({ theme }) => theme.colors.white};
  font-size: 1rem;
  font-weight: 600;
  margin-bottom: 0;

  @media ${({ theme }) => theme.device.mobileL} {
    font-size: 0.9rem;
  }
`

export const DayInfoNameEs = styled.p`
  color: ${({ theme }) => addOpacityToColor(theme.colors.white, 0.7)};
  font-size: 1rem;
  font-weight: 500;
  margin-bottom: 0;

  @media ${({ theme }) => theme.device.mobileL} {
    font-size: 0.9rem;
  }
`

export const DayInfoDate = styled.span`
  color: ${({ theme }) => addOpacityToColor(theme.colors.white, 0.5)};
  font-size: 0.9rem;
  font-weight: 200;

  @media ${({ theme }) => theme.device.mobileL} {
    font-size: 0.8rem;
  }
`
