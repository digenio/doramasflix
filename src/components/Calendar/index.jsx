import { useState } from 'react'
import { days } from 'utils/constans'
import { Day } from './Day'
import { CarrouselCalendar } from './styles'

export function Calendar ({ list, deviceType }) {
  const date = new Date()
  const [active, changeActive] = useState(date.getDay())

  const next = () => {
    if (active === 6) {
      changeActive(0)
    } else {
      changeActive(active + 1)
    }
  }

  const back = () => {
    if (active === 0) {
      changeActive(6)
    } else {
      changeActive(active - 1)
    }
  }

  if (deviceType === 'mobile') {
    return <Day day={days[active]} list={list} active next={next} back={back} />
  }

  return (
    <CarrouselCalendar>
      {days.map((day, idx) => (
        <Day day={day} key={day} list={list} active={active === idx} />
      ))}
    </CarrouselCalendar>
  )
}
