import Link from 'next/link'
import {
  DayItemCalendar,
  DayPoster,
  DayInfoName,
  DayInfoNameEs,
  DayInfo,
  DayInfoDate
} from './styles'
import Img from 'react-cool-img'
import { URL_IMAGE_POSTER_1X } from 'utils/urls'

export function DayItem ({
  name,
  name_es,
  slug,
  poster_path,
  schedule,
  poster
}) {
  const ini =
    schedule && schedule.startEmision
      ? new Date(schedule.startEmision)
      : undefined
  const end =
    schedule && schedule.endEmision ? new Date(schedule.endEmision) : undefined

  return (
    <Link href={'/doramas-online/[slug]'} as={`/${'doramas'}/${slug}`}>
      <DayItemCalendar>
        <DayPoster>
          <Img src={poster || URL_IMAGE_POSTER_1X + poster_path} width={80} />
        </DayPoster>
        <DayInfo>
          <DayInfoName>
            {name}{' '}
            {schedule && schedule.season > 1 && `Temporada ${schedule.season}`}
          </DayInfoName>
          <DayInfoNameEs>{name_es}</DayInfoNameEs>
          <DayInfoDate>
            {ini ? ini.toLocaleDateString() : 'Emisión'}
            {' - '}
            {end ? end.toLocaleDateString() : 'Actual'}
          </DayInfoDate>
        </DayInfo>
      </DayItemCalendar>
    </Link>
  )
}
