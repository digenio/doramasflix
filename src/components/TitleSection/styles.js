import styled from 'styled-components'
import { addOpacityToColor } from 'styles/utils'

export const Title = styled.div`
  position: relative;
  margin-bottom: 1rem;
  line-height: 1.875rem;
`

export const TitleName = styled.h2`
  color: ${({ theme }) => theme.colors.white};
  padding-left: 2rem;
  font-weight: 700;
  font-size: 1.125rem;
  margin-bottom: 0;
  padding: 5px 0;
  display: inline-block;
  vertical-align: top;
  margin-right: 0.5rem;
  padding-left: 1.7rem;
`

export const TitleH1 = styled.h1`
  color: ${({ theme }) => theme.colors.white};
  padding-left: 2rem;
  font-weight: 700;
  font-size: 1.125rem;
  margin-bottom: 0;
  padding: 5px 0;
  display: inline-block;
  vertical-align: top;
  margin-right: 0.5rem;
  padding-left: 1.7rem;
`

export const TitleIcon = styled.div`
  position: relative;
  svg {
    position: absolute;
    top: 7px;
  }
`

export const TitleLink = styled.a`
  margin-top: 10px;
  padding: 0 0.5rem;
  border-radius: 50px;
  line-height: 1.25rem;
  font-size: 0.75rem;
  box-shadow: inset 0 -10px 20px ${({ theme }) => addOpacityToColor(theme.colors.black, 0.3)};
  color: ${({ theme }) => theme.colors.white};
  background-color: ${({ theme }) => theme.colors.primary};
  border: 0;
  cursor: pointer;
  width: auto;
  display: inline-block;
  text-align: center;

  &:hover {
    transform: scale(1.1);
  }
`
