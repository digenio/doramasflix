import { colors } from 'styles/theme'
import { Title, TitleIcon, TitleName, TitleLink, TitleH1 } from './styles'
import Link from 'next/link'

export function TitleSection ({ name = '', title = '', Icon, url, children }) {
  return (
    <Title>
      <TitleIcon>{Icon && <Icon size={25} color={colors.primary} />}</TitleIcon>
      {name && <TitleName>{name}</TitleName>}
      {title && <TitleH1>{title}</TitleH1>}
      {url && (
        <Link href={url}>
          <TitleLink>Ver más</TitleLink>
        </Link>
      )}
      {children}
    </Title>
  )
}
