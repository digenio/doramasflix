import { Card } from '../Card'
import { Loading } from '../Loading'
import { URL_IMAGE_POSTER } from 'utils/urls'
import { ListContainer, ListItem } from './styles'

export function ListCast ({ items, loading }) {
  return (
    <ListContainer>
      {loading && <Loading />}
      {items.map((item, i) => {
        const type = item.dorama_id
          ? 'doramas-online'
          : item.movie_id
          ? 'peliculas-online'
          : 'pedidos'
        const label = item.dorama_id
          ? 'Dorama'
          : item.movie_id
          ? 'Pelicula'
          : ''
        const href = label ? `/${type}/[slug]` : `/${type}?=${item.name}`
        const as = label ? `/${type}/${item?.slug}` : href

        return (
          <ListItem key={i}>
            <Card
              href={href}
              as={as}
              image={URL_IMAGE_POSTER + item.poster_path}
              title={item.name}
              label={label}
            />
          </ListItem>
        )
      })}
    </ListContainer>
  )
}
