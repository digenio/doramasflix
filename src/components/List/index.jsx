import { Info } from '../Info'
import { Card } from '../Card'
import { Loading } from '../Loading'
import { URL_IMAGE_POSTER } from 'utils/urls'
import { ListContainer, ListItem } from './styles'

export function List ({ items, loading, hasInfo = true }) {
  return (
    <ListContainer>
      {loading && <Loading />}
      {items.map((item, i) => {
        const typeLocal =
          item.__typename === 'Dorama' ||
          item.type === 'dorama' ||
          item.type === 'tvshow'
            ? item.isTVShow || item.type === 'tvshow'
              ? 'variedades'
              : 'doramas'
            : 'peliculas'
        const label =
          typeLocal === 'variedades'
            ? 'TV Show'
            : typeLocal === 'doramas'
            ? 'Dorama'
            : 'Pelicula'
        const year =
          typeLocal === 'variedades'
            ? item.first_air_date && item.first_air_date.split('-')[0]
            : typeLocal === 'doramas'
            ? item.first_air_date && item.first_air_date.split('-')[0]
            : item.release_date && item.release_date.split('-')[0]
        return (
          <ListItem key={i}>
            <Card
              href={`/${typeLocal}-online/[slug]`}
              as={`/${typeLocal}-online/${item?.slug}`}
              image={item.poster || URL_IMAGE_POSTER + item.poster_path}
              title={item.name}
              langs={item.languages}
              label={label}
              year={year}
            >
              {hasInfo && <Info {...item} card />}
            </Card>
          </ListItem>
        )
      })}
    </ListContainer>
  )
}
