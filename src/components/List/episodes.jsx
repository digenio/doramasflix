import { SeenEpisode } from 'components/ActionsInfo/SeenEpisode'
import { CardEpisode } from 'components/Card/Episode'
import Link from 'next/link'
import { FaPlay } from 'react-icons/fa'
import { URL_IMAGE_EPISODE_1X } from 'utils/urls'
import {
  ListEpisodes,
  ListItem,
  ItemNumber,
  ItemInfo,
  ItemName,
  ItemDate,
  ImageItem,
  PlayItem,
  LeftItem,
  RightItem
} from './episodes.sty'

export function ListEpisodesDorama ({ list, listSeen }) {
  return (
    <ListEpisodes>
      {list.map((ep, i) => (
        <ListItem key={i}>
          <LeftItem>
            <ItemNumber>{i + 1}</ItemNumber>
            <ImageItem>
              <CardEpisode
                href={'/episodios/[slug]'}
                as={`/episodios/${ep?.slug}`}
                image={
                  ep.still_path
                    ? URL_IMAGE_EPISODE_1X + ep.still_path
                    : ep.backdrop
                }
              />
            </ImageItem>
            <ItemInfo>
              <Link href={'/episodios/[slug]'} as={`/episodios/${ep?.slug}`}>
                <a>
                  <ItemName>{`${
                    ep.season_number > 1 ? `Temporada ${ep.season_number}` : ''
                  } Episodio ${ep.episode_number}`}</ItemName>
                </a>
              </Link>
              <ItemDate>{ep.air_date && ep.air_date.split('T')[0]}</ItemDate>
            </ItemInfo>
          </LeftItem>
          <RightItem>
            <SeenEpisode
              _id={ep._id}
              listSeen={listSeen}
              serie_id={ep.serie_id}
            />
            <PlayItem>
              <Link href={'/episodios/[slug]'} as={`/episodios/${ep?.slug}`}>
                <a>
                  <FaPlay size={13} />
                </a>
              </Link>
            </PlayItem>
          </RightItem>
        </ListItem>
      ))}
    </ListEpisodes>
  )
}
