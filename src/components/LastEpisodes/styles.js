import styled from 'styled-components'

export const List = styled.ul`
  margin-bottom: 1.6rem;
  margin-left: -0.4rem;
  margin-right: -0.4rem;
  margin: 0;
  padding: 0;
  list-style-type: none;
  display: flex;
  overflow: auto;

  @media ${({ theme }) => theme.device.laptop} {
    flex-wrap: wrap;
    overflow: none;
  }
`

export const ListHorizontal = styled.ul`
  margin-bottom: 1.6rem;
  margin-left: -0.4rem;
  margin-right: -0.4rem;
  margin: 0;
  padding: 0;
  list-style-type: none;
  display: flex;
  overflow: auto;
  margin-bottom: 20px;
`

export const Item = styled.li`
  padding: 0 0.3rem 0.6rem;
  min-width: 0;
  flex: 0 0 70%;
  max-width: 70%;

  @media ${({ theme }) => theme.device.mobileL} {
    flex: 0 0 45%;
    max-width: 45%;
  }

  @media ${({ theme }) => theme.device.tablet} {
    flex: 0 0 30%;
    max-width: 30%;
  }

  @media ${({ theme }) => theme.device.laptopL} {
    flex: 0 0 33.3333%;
    max-width: 33.3333%;
  }

  @media ${({ theme }) => theme.device.laptopL} {
    flex: 0 0 25%;
    max-width: 25%;
  }

  @media ${({ theme }) => theme.device.desktop} {
    flex: 0 0 20%;
    max-width: 20%;
  }
`

export const ItemSeen = styled.li`
  padding: 0 0.3rem 0.6rem;
  min-width: 0;
  flex: 0 0 60%;
  max-width: 60%;

  @media ${({ theme }) => theme.device.mobileL} {
    flex: 0 0 40%;
    max-width: 40%;
  }

  @media ${({ theme }) => theme.device.tablet} {
    flex: 0 0 30%;
    max-width: 30%;
  }

  @media ${({ theme }) => theme.device.laptop} {
    flex: 0 0 24%;
    max-width: 24%;
  }

  @media ${({ theme }) => theme.device.laptopL} {
    flex: 0 0 18%;
    max-width: 18%;
  }

  @media ${({ theme }) => theme.device.desktop} {
    flex: 0 0 15%;
    max-width: 15%;
  }
`
