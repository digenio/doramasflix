import { ListHorizontal, ItemSeen } from './styles'
import { URL_IMAGE_EPISODE_1X, URL_IMAGE_EPISODE_2X } from 'utils/urls'
import { createArray } from 'utils/functions'
import { CardLoader } from 'components/Loader/Card'
import { CardEpisode } from 'components/Card/Episode'

export function LastSeenEpisodes ({ episodes = [], loading, deviceType }) {
  const arrayLoading = createArray(12)
  return (
    <ListHorizontal>
      {loading
        ? arrayLoading.map(it => (
            <ItemSeen key={it}>
              <CardLoader width={'100%'} height={50} />
            </ItemSeen>
          ))
        : episodes.map(
            (ep, i) =>
              ep && (
                <ItemSeen key={i}>
                  <CardEpisode
                    href={'/episodios/[slug]'}
                    as={`/episodios/${ep?.slug}`}
                    height={deviceType !== 'laptop' ? '120px' : '150px'}
                    title={`${ep?.serie_name} ${
                      ep?.season_number > 1
                        ? `Temporada ${ep?.season_number}`
                        : ''
                    } Episodio ${ep?.episode_number}`}
                    image={
                      ep?.still_path
                        ? URL_IMAGE_EPISODE_1X + ep?.still_path
                        : null
                    }
                    srcSet={`${URL_IMAGE_EPISODE_1X}${ep?.still_path} 1x, ${URL_IMAGE_EPISODE_2X}${ep?.still_path} 2x`}
                    coverImage
                  />
                </ItemSeen>
              )
          )}
    </ListHorizontal>
  )
}
