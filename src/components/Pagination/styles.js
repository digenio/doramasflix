import styled from 'styled-components'
import { addOpacityToColor } from 'styles/utils'

export const PaginationContainer = styled.div`
  margin-bottom: 1rem;
  ul {
    display: flex;
    padding: 0;
    margin: 0;
    max-width: 100%;
    flex-wrap: wrap;
  }

  li {
    cursor: pointer;
    background-color: ${({ theme }) => theme.colors.gray};
    border-radius: 3px;
    box-shadow: inset 0 0 70px
        ${({ theme }) => addOpacityToColor(theme.colors.black, 0.3)},
      0 0 20px ${({ theme }) => addOpacityToColor(theme.colors.black, 0.5)};
    margin: 0 3px 7px;

    &.disabled {
      opacity: 0.5;
    }

    &.selected {
      background-color: ${({ theme }) => theme.colors.primary};
    }

    &:hover {
      background-color: ${({ theme }) =>
        addOpacityToColor(theme.colors.primary, 0.8)};
      &.disabled {
        background-color: ${({ theme }) => theme.colors.gray};
        opacity: 0.5;
      }
    }
  }

  a {
    width: 100%;
    padding-left: 1rem;
    padding-right: 1rem;
    display: inline-block;
    vertical-align: top;

    min-width: 50px;
    height: 50px;
    line-height: 50px;
    font-size: 1rem;
    font-weight: 700;
    text-align: center;
    color: ${({ theme }) => theme.colors.white};
    position: relative;

    svg {
      position: absolute;
      top: 15px;
      left: 15px;
    }
  }
`
