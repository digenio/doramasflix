import { FiltersContainer } from './styles'

import { GenreFilter } from './Genre'
import { NetworkFilter } from './Network'
import { CountryFilter } from './Country'
import { SortFilter } from './Sort'
import { LangFilter } from './Lang'
import { useState } from 'react'

export function Filters ({
  filter,
  changeFilter,
  sort,
  changeSort,
  changePage
}) {
  const [menu, changeMenu] = useState('')
  return (
    <FiltersContainer>
      {filter && (
        <>
          <GenreFilter
            filter={filter}
            changeFilter={changeFilter}
            menu={menu}
            changeMenu={changeMenu}
            changePage={changePage}
          />
          <NetworkFilter
            filter={filter}
            changeFilter={changeFilter}
            menu={menu}
            changeMenu={changeMenu}
            changePage={changePage}
          />
          <CountryFilter
            filter={filter}
            changeFilter={changeFilter}
            menu={menu}
            changeMenu={changeMenu}
            changePage={changePage}
          />
          <LangFilter
            filter={filter}
            changeFilter={changeFilter}
            menu={menu}
            changeMenu={changeMenu}
            changePage={changePage}
          />
        </>
      )}
      {sort && (
        <SortFilter
          sort={sort}
          changeSort={changeSort}
          menu={menu}
          changeMenu={changeMenu}
          changePage={changePage}
        />
      )}
    </FiltersContainer>
  )
}
