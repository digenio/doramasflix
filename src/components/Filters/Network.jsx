import { useState } from 'react'
import { useQuery } from '@apollo/react-hooks'
import { FaProductHunt } from 'react-icons/fa'
import { MdArrowDropDown } from 'react-icons/md'

import { CheckBox } from '../CheckBox'
import { NETWORK_LIST } from 'gql/network'
import {
  FilterItem,
  FilterSpan,
  FilterIcon,
  FilterChoise,
  FilterIconDown,
  FilterNav,
  FilterNavList,
  FilterNavItem,
  FilterNavText
} from './styles'

export function NetworkFilter ({
  filter = {},
  changeFilter,
  menu,
  changeMenu,
  changePage
}) {
  const { data: { listNetworks = [] } = {} } = useQuery(NETWORK_LIST, {
    variables: { sort: 'NUMBER_OF_DORAMAS_DESC', limit: 0, skip: 0 }
  })

  const { networkIds = [] } = filter

  const handleChange = (checked, id) => {
    if (checked) {
      networkIds.push(id)
    } else {
      const index = networkIds.findIndex(item => item === id)
      networkIds.splice(index, 1)
    }
    if (networkIds.length === 0) {
      changeFilter({ ...filter, networkIds: undefined })
    } else {
      changeFilter({ ...filter, networkIds })
    }
    changePage(1)
  }

  const visible = menu === 'network'
  const open = () => changeMenu('network')
  const close = () => changeMenu('')
  const toggle = visible ? close : open

  return (
    <FilterItem>
      <FilterIcon>
        <FaProductHunt size={20} />
      </FilterIcon>
      <FilterSpan>Productoras:</FilterSpan>
      <FilterChoise active={networkIds.length > 0} onClick={toggle}>
        Elegir
        <FilterIconDown>
          <MdArrowDropDown size={30} />
        </FilterIconDown>
      </FilterChoise>
      <FilterNav visible={visible}>
        <FilterNavList>
          {listNetworks.map(({ _id, name }) => (
            <FilterNavItem key={_id}>
              <CheckBox
                value={_id}
                checked={!!networkIds.find(item => item === _id)}
                onChange={handleChange}
              />
              <FilterNavText>{name}</FilterNavText>
            </FilterNavItem>
          ))}
        </FilterNavList>
      </FilterNav>
    </FilterItem>
  )
}
