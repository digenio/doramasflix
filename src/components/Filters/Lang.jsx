import { FaLanguage } from 'react-icons/fa'
import { MdArrowDropDown } from 'react-icons/md'

import { CheckBox } from '../CheckBox'
import { languagesList } from 'utils/constans'
import {
  FilterItem,
  FilterSpan,
  FilterIcon,
  FilterChoise,
  FilterIconDown,
  FilterNav,
  FilterNavList,
  FilterNavItem,
  FilterNavText
} from './styles'

export function LangFilter ({
  menu,
  changeMenu,
  filter = {},
  changeFilter,
  changePage
}) {
  const { bylanguages = [] } = filter

  const handleChange = (checked, id) => {
    if (checked) {
      bylanguages.push(id)
    } else {
      const index = bylanguages.findIndex(item => item === id)
      bylanguages.splice(index, 1)
    }
    if (bylanguages.length === 0) {
      changeFilter({ ...filter, bylanguages: undefined })
    } else {
      changeFilter({ ...filter, bylanguages })
    }
    changePage(1)
  }

  const visible = menu === 'lang'
  const open = () => changeMenu('lang')
  const close = () => changeMenu('')
  const toggle = visible ? close : open

  return (
    <FilterItem>
      <FilterIcon>
        <FaLanguage size={22} />
      </FilterIcon>
      <FilterSpan>Idioma:</FilterSpan>
      <FilterChoise active={bylanguages.length > 0} onClick={toggle}>
        Elegir
        <FilterIconDown>
          <MdArrowDropDown size={30} />
        </FilterIconDown>
      </FilterChoise>
      <FilterNav visible={visible}>
        <FilterNavList>
          {languagesList.map(({ id, name }) => (
            <FilterNavItem key={id}>
              <CheckBox
                value={id}
                checked={!!bylanguages.find(item => item === id)}
                onChange={handleChange}
              />
              <FilterNavText>{name}</FilterNavText>
            </FilterNavItem>
          ))}
        </FilterNavList>
      </FilterNav>
    </FilterItem>
  )
}
