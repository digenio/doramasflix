import React from 'react'
import { Share, Title, Buttons } from './styles'
import {
  FacebookIcon,
  FacebookShareButton,
  TelegramIcon,
  TelegramShareButton,
  TwitterIcon,
  TwitterShareButton,
  WhatsappIcon,
  WhatsappShareButton
} from 'react-share'

export function ShareEpisode ({ url, name }) {
  const title = `Estoy viendo ${name} en DoramasFlix, ¿Te animás a verlo conmigo?`
  return (
    <Share>
      <Title>Compartir</Title>
      <Buttons>
        <FacebookShareButton url={url} quote={title}>
          <FacebookIcon size={40} round />
        </FacebookShareButton>
        <TwitterShareButton url={url} title={title}>
          <TwitterIcon size={40} round />
        </TwitterShareButton>
        <TelegramShareButton url={url} title={title}>
          <TelegramIcon size={40} round />
        </TelegramShareButton>
        <WhatsappShareButton url={url} title={title} separator=':: '>
          <WhatsappIcon size={40} round />
        </WhatsappShareButton>
      </Buttons>
    </Share>
  )
}
