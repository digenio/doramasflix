import styled, { css } from 'styled-components'
import { addOpacityToColor } from 'styles/utils'

export const SortBy = styled.div`
  display: block;
  transition: 0.2s;
  height: 1.875rem;
  line-height: 1.875rem;
  position: relative;
  margin-top: 5px;
  white-space: nowrap;

  &::after {
    content: '';
    display: inline-block;
    vertical-align: top;
    width: 0;
    height: 0;
    border-left: 4px solid transparent;
    border-right: 4px solid transparent;
    border-top: 4px solid transparent;
    margin-top: 0.9rem;
    margin-left: -1.3rem;
    border-top-color: ${({ theme }) => theme.colors.primary};
  }

  @media ${({ theme }) => theme.device.mobileL} {
    display: inline-block;
    margin-left: 0.5rem;
  }
  @media ${({ theme }) => theme.device.tablet} {
    position: absolute;
    right: 0.8rem;
    top: 0;
  }
`

export const SortBySpan = styled.span`
  display: inline-block;
  vertical-align: top;
  position: relative;
  z-index: 1;
  padding-right: 0.3rem;
`

export const SortByList = styled.ul`
  margin-left: -1.5rem;
  margin-top: -0.5rem;
  padding: 0.5rem 0;
  border-radius: 6px;
  display: inline-block;
  vertical-align: top;
  position: relative;
  z-index: 1;
  list-style-type: none;

  ${({ open }) =>
    open &&
    css`
      box-shadow: inset 0 0 70px
          ${({ theme }) => addOpacityToColor(theme.colors.black, 0.3)},
        0 0 20px ${({ theme }) => addOpacityToColor(theme.colors.black, 0.5)};
      background-color: ${({ theme }) => theme.colors.bg};
      z-index: 3;
      li {
        opacity: 1;
        max-height: 9em;
        display: block;
      }
    `}
`

export const SortByListItem = styled.li`
  max-height: 0;
  overflow: hidden;
  opacity: 0;
  display: none;
  transition: 0.2s;

  ${({ active }) =>
    active &&
    css`
      font-weight: 700;
      opacity: 1;
      max-height: 9em;
      display: block;
    `}

  ${({ active, open }) =>
    open &&
    active &&
    css`
      a {
        color: ${({ theme }) => theme.colors.primary};
      }
    `}
`

export const SortByListLink = styled.a`
  padding: 0 1.5rem;
  position: relative;
  display: inline-block;
  color: ${({ theme }) => theme.colors.white};
  transition: 0.2s;
`
