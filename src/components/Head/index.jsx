import HeadNext from "next/head";

export function Head({ title, description, image, url, extra }) {
  return (
    <HeadNext>
      <title>
        {title || "Doramasflix ⚜️ Ver Doramas Online Gratis Completas HD ✔️"}
      </title>
      <link rel="canonical" href={url || "https://doramasflix.in/"} />
      <meta property="og:url" content={url || "https://doramasflix.in/"} />

      <meta
        name="description"
        content={
          description ||
          " En ⚡️ Doramasflix ⚡️ puedes ✓ Ver doramas online gratis HD ✓ Peliculas Gratis. Ver Dramas, Episodios y Temporadas Completas actualizadas en HD."
        }
      />
      <meta
        property="og:title"
        content={
          title || "Doramasflix ⚜️ Ver Doramas Online Gratis Completas HD ✔️"
        }
      />
      <meta
        property="og:description"
        content={
          description ||
          " En ⚡️ Doramasflix ⚡️ puedes ✓ Ver doramas online gratis HD ✓ Peliculas Gratis. Ver Dramas, Episodios y Temporadas Completas actualizadas en HD."
        }
      />
      <meta property="og:image" content={image || "/img/meta.jpg"} />
      <meta name="twitter:card" content={image || "/img/meta.jpg"} />
      <meta
        name="twitter:title"
        content={
          title || "Doramasflix ⚜️ Ver Doramas Online Gratis Completas HD ✔️"
        }
      />
      <meta
        name="twitter:description"
        content={
          description ||
          " En ⚡️ Doramasflix ⚡️ puedes ✓ Ver doramas online gratis HD ✓ Peliculas Gratis. Ver Dramas, Episodios y Temporadas Completas actualizadas en HD."
        }
      />
      <meta
        name="viewport"
        content="width=device-width,minimum-scale=1,initial-scale=1"
      />
      <script async src="https://arc.io/widget.min.js#6qHrnJrh"></script>
      {extra}

      <meta name="google-play-app" content="app-id=com.asiapp.doramasgo" />

      <link rel="android-touch-icon" href="/img/ic_launcher.png" />
    </HeadNext>
  );
}
