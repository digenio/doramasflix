import styled from 'styled-components'

export const Networks = styled.article`
  margin-bottom: 2rem;
`

export const NetworksHeader = styled.header`
  margin-bottom: 1rem;
  line-height: 1.875rem;
`

export const Networkstitle = styled.h1`
  font-weight: 700;
  font-size: 2rem;
  margin-bottom: 0;
  padding: 5px 0;
  display: inline-block;
  vertical-align: top;
  margin-right: 0.5rem;
  color: ${({ theme }) => theme.colors.white};
`

export const ListNetworks = styled.div`
  display: block;
`

export const NetworkSubTitle = styled.h2`
  color: ${({ theme }) => theme.colors.white};

  &:hover {
    color: ${({ theme }) => theme.colors.primary};
  }
`

export const Network = styled.div`
  margin-top: 0;
`

export const NetworkText = styled.p`
  margin-top: 0;
`

export const NetworkTextDiv = styled.div`
  margin-top: 0;
`

export const NetworkLink = styled.a`
  margin-top: 0;
`
