import {
  Network,
  Networks,
  NetworkText,
  ListNetworks,
  Networkstitle,
  NetworkTextDiv,
  NetworksHeader,
  NetworkSubTitle
} from './styles'
import Link from 'next/link'

export function NetworksList ({ list }) {
  return (
    <Networks>
      <NetworksHeader>
        <Networkstitle>Lista de Productoras de doramas</Networkstitle>
      </NetworksHeader>
      <ListNetworks>
        <NetworkText>
          En Doramasflix, te ofrecemos una lista extensa de las productoras de
          doramas, que están disponibles en la plataforma. Las productoras de
          televisión coreana, te la compartimos en la siguiente línea:
        </NetworkText>
        {list.map(({ name, slug, description = '' }, idx) => (
          <Network key={idx}>
            <Link href='/productoras/[slug]' as={`/productoras/${slug}`}>
              <a>
                <NetworkSubTitle>{`- Doramas de ${name}`}</NetworkSubTitle>
              </a>
            </Link>
            <NetworkTextDiv dangerouslySetInnerHTML={{ __html: description }} />
          </Network>
        ))}
      </ListNetworks>
    </Networks>
  )
}
