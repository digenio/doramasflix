import styled from 'styled-components'

export const TermsContainer = styled.article`
  a,
  h1,
  h2,
  h3 {
    color: ${({ theme }) => theme.colors.white};
  }
`
