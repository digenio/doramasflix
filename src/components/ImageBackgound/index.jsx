import { URL_IMAGE_BACK_DROPS_1X, URL_IMAGE_BACK_DROPS_2X } from 'utils/urls'
import { Image, ImageFigure, ImageBackgroundContainer } from './styles'

export function ImageBackground ({ backdrop_path = '', backdrop }) {
  return (
    <ImageBackgroundContainer>
      <ImageFigure>
        <Image
          src={backdrop || URL_IMAGE_BACK_DROPS_1X + backdrop_path}
          srcSet={
            !backdrop &&
            `${URL_IMAGE_BACK_DROPS_1X}${backdrop_path} 1x, ${URL_IMAGE_BACK_DROPS_2X}${backdrop_path} 2x`
          }
        />
      </ImageFigure>
    </ImageBackgroundContainer>
  )
}
