import React from 'react'
import dynamic from 'next/dynamic'
import { TitleSection } from 'components/TitleSection'
import { MdMovieFilter } from 'react-icons/md'
import { useNearScreen } from 'hooks/useNearScreen'

const CastList = dynamic(() => import('./CastList'))

export function Cast ({ cast, name }) {
  const { isNearScreen, fromRef } = useNearScreen({
    distance: '100px'
  })

  return (
    <div style={{ minHeight: 150 }} ref={fromRef}>
      <TitleSection name={`Reparto de ${name}`} Icon={MdMovieFilter} />
      {isNearScreen && <CastList cast={cast} />}
    </div>
  )
}
