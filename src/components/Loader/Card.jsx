import React from 'react'
import ContentLoader from 'react-content-loader'

import { colors } from 'styles/theme'

export function CardLoader ({ height = 165, width = 100, ...props }) {
  const coverHeight = height
  const coverWidth = width
  const speed = 2

  return (
    <ContentLoader
      speed={speed}
      width={coverWidth}
      height={coverHeight}
      backgroundColor={colors.backg}
      foregroundColor={colors.foreng}
      {...props}
    >
      <rect x='0' y='0' rx='0' ry='0' width={coverWidth} height={coverHeight} />
    </ContentLoader>
  )
}
