import React from 'react'
import ContentLoader from 'react-content-loader'

import { colors } from 'styles/theme'
import { Container } from 'styles/base'

export function CarrouselLoader (props) {
  return (
    <Container>
      <ContentLoader
        height={440}
        width={'100%'}
        speed={2}
        backgroundColor={colors.backg}
        foregroundColor={colors.foreng}
        {...props}
      >
        <rect x='0' y='156' rx='3' ry='3' width='210' height='12' />
        <rect x='0' y='180' rx='3' ry='3' width='280' height='8' />
        <rect x='0' y='198' rx='3' ry='3' width='178' height='6' />
        <rect x='0' y='215' rx='3' ry='3' width='410' height='6' />
        <rect x='0' y='225' rx='3' ry='3' width='410' height='6' />
        <rect x='0' y='235' rx='3' ry='3' width='410' height='6' />
        <rect x='0' y='245' rx='3' ry='3' width='410' height='6' />
        <rect x='0' y='270' rx='3' ry='3' width='250' height='8' />
        <rect x='0' y='285' rx='3' ry='3' width='250' height='8' />
        <rect x='0' y='300' rx='3' ry='3' width='250' height='8' />
        <rect x='0' y='330' rx='3' ry='3' width='150' height='30' />
      </ContentLoader>
    </Container>
  )
}
