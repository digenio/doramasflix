import { FaSadCry } from 'react-icons/fa'

import {
  NotTop,
  NotIcon,
  NotTopText,
  NotTopTitle,
  NotContainer
} from './styles'

export function NotFound ({ title }) {
  return (
    <NotContainer>
      <NotIcon>
        <FaSadCry size={50} />
        <NotTop>
          <NotTopTitle>{title}</NotTopTitle>
          <NotTopText>
            Lo sentimos, la página que buscas no existe. Tal vez lo que buscas
            estás en las siguientes categorías mostradas a continuación.
          </NotTopText>
          <NotTopText>
            No olvides usar el buscador para buscar tu serie favorita.
          </NotTopText>
        </NotTop>
      </NotIcon>
    </NotContainer>
  )
}
