import Link from 'next/link'

import { Widget } from '../Widget'
import { countries } from 'utils/constans'
import { WidgetList, WidgetItem, WidgetImg } from './styles'

export default function CountryList () {
  return (
    <Widget title='Paises'>
      <WidgetList>
        {countries.map(({ slug, name }, i) => (
          <WidgetItem key={i}>
            <Link href='/paises/[slug]' as={`/paises/${slug}`}>
              <a className='WidgetLink'>{name}</a>
            </Link>
            <WidgetImg src={`/img/lang/${slug}.svg`} width='20' height='20' />
          </WidgetItem>
        ))}
      </WidgetList>
    </Widget>
  )
}
