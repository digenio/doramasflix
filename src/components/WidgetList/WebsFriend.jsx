import { WidgetList, WidgetItem } from './styles'
import { Widget } from '../Widget'
import { webs } from 'utils/constans'

export function WebsFriend () {
  return (
    <Widget title='Webs Amigas'>
      <WidgetList>
        {webs.map((web, i) => (
          <WidgetItem key={i}>
            <a href={web.url}>{web.name}</a>
          </WidgetItem>
        ))}
      </WidgetList>
    </Widget>
  )
}
