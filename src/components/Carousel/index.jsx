import CarouselLib from 'react-multi-carousel'

import { Item } from './Item'
import { URL_IMAGE_POSTER_X } from 'utils/urls'
import { CarouselContainer, Dot, Poster } from './styles'

export function Carousel ({ list, deviceType }) {
  const CustomDot = ({ index, onClick, active }) => {
    return (
      <Dot
        onClick={e => {
          onClick()
          e.preventDefault()
        }}
        active={active}
      >
        <Poster
          onClick={e => {
            onClick()
            e.preventDefault()
          }}
          src={
            list[index] && list[index].poster
              ? list[index].poster
              : URL_IMAGE_POSTER_X +
                (list[index] ? list[index].poster_path : '')
          }
          imgactive={active ? 'active' : ''}
        />
      </Dot>
    )
  }

  return (
    <CarouselContainer>
      <CarouselLib
        ssr
        autoPlay
        infinite
        showDots
        draggable
        swipeable
        focusOnSelect
        renderDotsOutside
        arrows={false}
        slidesToSlide={1}
        keyBoardControl={false}
        responsive={responsive}
        deviceType={deviceType}
        customDot={<CustomDot />}
      >
        {list.map(dorama => {
          return <Item key={dorama._id} {...dorama} notList />
        })}
      </CarouselLib>
    </CarouselContainer>
  )
}

const responsive = {
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 1
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 1
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1
  }
}
