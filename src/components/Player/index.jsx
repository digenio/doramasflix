import Link from "next/link";
import { useContext, useState } from "react";
import { MdPlaylistPlay } from "react-icons/md";
import { FaStepBackward, FaStepForward } from "react-icons/fa";
import { FaTelegramPlane } from "react-icons/fa";
import { Container } from "styles/base";
import { PlayerModal } from "./Modal";
import { ImageBackground } from "../ImageBackgound";
import {
  Video,
  NoOptions,
  VideoFrame,
  VideoTitle,
  PlayerVideo,
  VideoControl,
  VideoControls,
  PlayerContainer,
  VideoControlText,
  PlayerVideoOptions,
  NoOptionsText,
} from "./styles";
import { Button } from "components/Info/styles";
import { Space } from "components/Space";
import { Iframe } from "./iframe";
import { PlayerOptionsPicker } from "./options";
import { getDateString, isBeforeDateMargin2 } from "utils/functions";
import { ServerContext } from "contexts/ServerContext";

export function Player({
  next,
  prev,
  slug,
  title,
  links,
  backdrop_path,
  episode,
  air_date,
  sources = [],
  _id,
  dorama,
  countDownDate,
  season,
}) {
  const [optionActual, changeOption] = useState();
  const [visible, changeVisible] = useState(false);

  const { servers } = useContext(ServerContext);

  const { pause, emision, uploading, commingSoon } = season || {};

  const isBeforeDate = links.length > 0 ? false : air_date && isBeforeDateMargin2(air_date);

  const { premiere, schedule } = dorama || {};

  //const serverCurrent = servers.find((it) => it.code_flix === optionActual?.server);

  //const link = optionActual?.link ? `${optionActual?.link}${serverCurrent ? `?name=${serverCurrent.name}` : ""}` : "";

  const telegranButtom = () => (
    <>
      <Space height="30px" />
      <span style={{ fontSize: 14 }}>
        Unete a nuestro canal de Telegram para estar más informado de cuando se suben nuevos episodios.
      </span>
      <Space height="10px" />
      <Button noUpper color="#0088cc" maxWidth="200px" href="https://t.me/doramasflixgo" target="_blank">
        <FaTelegramPlane size={16} />
        @doramasflixgo
      </Button>
    </>
  );

  const pauseRender = () => (
    <NoOptions>
      <NoOptionsText>Temporada actualmente en pausa</NoOptionsText>
      Los doramas en pausa se debe a que el dorama se dejo de emitir, se retraso en su trasmisión o no se han encontrado
      los subtitulos del mismo. Puedes disfrutar de otros doramas en la app o la web mientras esperas a que se suban
      nuevos episodios.
      <Space height="10px" />
      {telegranButtom()}
    </NoOptions>
  );

  const uploadingRender = () => (
    <NoOptions>
      <NoOptionsText>Temporada actualmente subiendose poco a poco</NoOptionsText>
      Las temporadas que se suben poco a poco se debe a que la temporada se retraso en su trasmisión o no se han
      encontrado los subtitulos del mismo. Puedes disfrutar de otros doramas en la app o la web mientras esperas a que
      se suban nuevos episodios.
      <Space height="10px" />
      {telegranButtom()}
    </NoOptions>
  );

  const commingSoonRender = () => (
    <NoOptions>
      <NoOptionsText>Próximamente</NoOptionsText>
      Las temporada se estrenara próximamente. Puedes disfrutar de otros doramas en la app o la web mientras esperas a
      que se suban nuevos episodios.
      <Space height="10px" />
      {telegranButtom()}
    </NoOptions>
  );

  const beforeDateRender = () => (
    <NoOptions>
      <NoOptionsText>El episodio estará disponible el {getDateString(air_date)}</NoOptionsText>
      Los doramas en emisión se publican después de 5 a 6 horas luego de su estreno en Asia. En ciertos casos, puede
      haber retrasos de un día o en casos excepcionales hasta una semana, por retrasos del drama o inconvenientes con el
      equipo de traducción.
      <Space height="10px" />
      {telegranButtom()}
    </NoOptions>
  );

  const premiereRender = () => (
    <NoOptions>
      <NoOptionsText>El dorama se encuentra en emisión</NoOptionsText>
      <NoOptionsText>
        El dorama se entrenó el {getDateString(schedule?.startEmision)} y se emite los dias {schedule?.days?.join(", ")}
        .
      </NoOptionsText>
      Los doramas en emisión se publican después de 5 a 6 horas luego de su estreno en Asia. En ciertos casos, puede
      haber retrasos de un día o en casos excepcionales hasta una semana, por retrasos del drama o del equipo de
      traducción. Para más información, únete por favor a nuestro grupo oficial en Telegram.
      <Space height="10px" />
      {telegranButtom()}
    </NoOptions>
  );

  const notOptionsRender = () => (
    <NoOptions>
      <NoOptionsText>No hay opciones disponibles</NoOptionsText>
      Reportalo para que podamos solucionarlo lo mas pronto posible. ó reportalo en nuestro telegram!
      <Space height="10px" />
      {telegranButtom()}
    </NoOptions>
  );

  return (
    <PlayerContainer>
      <Container>
        <PlayerVideoOptions>
          <PlayerVideo>
            <VideoTitle>{title}</VideoTitle>
            <Video>
              <VideoFrame>
                {optionActual ? (
                  <Iframe src={optionActual?.link} type="text/html" allowFullScreen frameborder="0" />
                ) : pause ? (
                  pauseRender()
                ) : uploading ? (
                  uploadingRender()
                ) : commingSoon ? (
                  commingSoonRender()
                ) : isBeforeDate ? (
                  beforeDateRender()
                ) : emision ? (
                  premiereRender()
                ) : (
                  notOptionsRender()
                )}
              </VideoFrame>
            </Video>
            <VideoControls>
              <Link href="/episodios/[slug]" as={`/episodios/${prev && prev?.slug}`}>
                <VideoControl disabled={!prev}>
                  <FaStepBackward size={15} />
                  <VideoControlText> Anterior</VideoControlText>
                </VideoControl>
              </Link>
              <Link href="/doramas-online/[slug]" as={`/doramas-online/${slug}`}>
                <VideoControl>
                  <MdPlaylistPlay size={25} />
                  <VideoControlText> Lista de episodios</VideoControlText>
                </VideoControl>
              </Link>
              <Link href="/episodios/[slug]" as={`/episodios/${next && next?.slug}`}>
                <VideoControl disabled={!next}>
                  <VideoControlText>Siguiente </VideoControlText>
                  <FaStepForward size={15} />
                </VideoControl>
              </Link>
            </VideoControls>
          </PlayerVideo>
          <PlayerOptionsPicker
            links_online={links.map((it, idx) => ({
              ...it,
              index: idx,
            }))}
            server={optionActual}
            changeServer={changeOption}
            isBeforeDate={isBeforeDate}
            season={season}
            premiere={premiere}
            sources={sources}
            changeVisible={changeVisible}
            _id={_id}
          />
        </PlayerVideoOptions>
        {backdrop_path && <ImageBackground backdrop_path={backdrop_path} />}
        <PlayerModal visible={visible} changeVisible={changeVisible} episode={episode} option={optionActual} />
      </Container>
    </PlayerContainer>
  );
}
