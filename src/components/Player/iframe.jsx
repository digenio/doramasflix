import React, { useEffect, useRef } from 'react'
import { PlayerIFrame } from './styles'

export function Iframe ({ src, referer, origin, type, ...props }) {
  const iframe = useRef()

  useEffect(() => {
    if (referer) {
      fetch('https://cors.seriesapi.co/' + src, {
        headers: {
          referer,
          origin
        }
      })
        .then(response => response.blob())
        .then(response => {
          var blob = new Blob([response], { type })
          const obj = URL.createObjectURL(blob)
          iframe.current.src = obj
        })
        .catch(e => console.error('Error', e))
    } else {
      iframe.current.src = src
    }
  }, [src, referer])

  return <PlayerIFrame {...props} ref={iframe} />
}
