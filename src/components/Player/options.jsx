import { useState, useEffect, useContext } from "react";
import { AiFillAlert } from "react-icons/ai";
import { FaPlay } from "react-icons/fa";

import {
  languages,
  servers,
  langsName,
  serversAvalibles,
  langsNameAb,
} from "utils/constans";
import {
  PlayerImage,
  PlayerOption,
  PlayerOptions,
  PlayerOptionText,
  PlayerOptionsCont,
  PlayerOptionLink,
  PlayerOptionTitle,
  PlayerOptionReport,
  PlayerOptionSubTitle,
  PlayerLangs,
  PlayerLang,
  PlayerImageLang,
} from "./styles";
import { useOptions } from "hooks/useOptions";
import { LanguageContext } from "contexts/LanguageContext";
import { ServerContext } from "contexts/ServerContext";
import { Loading } from "components/Loading";

export function PlayerOptionsPicker({
  links_online = [],
  server,
  _id,
  changeServer,
  isBeforeDate,
  sources = [],
  changeVisible,
  premiere,
  season,
}) {
  const [lang, changeLang] = useState();
  const { languages } = useContext(LanguageContext);
  const { servers } = useContext(ServerContext);

  const { options, langs, loading } = useOptions({
    _id,
    type: "episode",
    links_online,
    changeLang,
    changeServer,
    lang,
  });

  // useEffect(() => {
  //   if (server) {
  //     changeLoading(false)
  //   }
  //   if (!loading) {
  //     changeLoading(false)
  //   }
  // }, [server, loading])

  // const [lang, changeLang] = useState()
  // const [options, changeOptions] = useState([])
  // const [langs, changeLanguages] = useState([])

  // useEffect(() => {
  //   let opts = []
  //   let lngs = []

  //   serversAvalibles.forEach(server => {
  //     opts = [...opts, ...links_online.filter(l => l.server == server)]

  //     let drama = opts.filter(it => it.page === 'doramasflix')
  //     let others = opts.filter(it => it.page !== 'doramasflix')

  //     opts = [...drama, ...others]
  //   })
  //   opts.forEach((op, idx) => {
  //     op.idx = idx
  //     const index = lngs.findIndex(it => it === op.lang)
  //     if (index === -1) {
  //       lngs.push(op.lang)
  //     }
  //   })
  //   if (sources.length > 0) {
  //     opts = [
  //       ...sources.map(it => ({
  //         ...it,
  //         link: `https://flixplayer.xyz/doramasflix/${_id}?server=${it.server}&lang=${it.lang}`
  //       })),
  //       ...opts
  //     ]
  //   }

  //   changeOptions(opts)
  //   changeLanguages(lngs)

  //   const lng = lngs[0]

  //   //const opt = opts.find(s => s.lang === lng)
  //   //changeServer(opt)
  //   changeLang(lng)
  // }, [_id])

  // useEffect(() => {
  //   const opt = options.find(s => s.lang === lang)
  //   if (opt) {
  //     changeServer(opt)
  //   } else {
  //     const opt = options.find(s => s.lang === lang)
  //     changeServer(opt)
  //   }
  // }, [lang])
  const { pause, uploading, commingSoon } = season || {};

  const avalibleReports =
    !pause &&
    !uploading &&
    !commingSoon &&
    (links_online.length > 0 || !isBeforeDate);

  return (
    <PlayerOptionsCont>
      <PlayerOption>
        <PlayerOptionTitle>
          <FaPlay size={16} />
          <PlayerOptionSubTitle>Opciones</PlayerOptionSubTitle>
        </PlayerOptionTitle>
      </PlayerOption>
      <PlayerLangs>
        {langs.map((lang, idx) => {
          const language = languages.find((it) => it.code_flix == lang);
          if (!language) {
            return null;
          }
          return (
            <PlayerLang
              key={idx}
              onClick={() => changeLang(lang)}
              active={lang === server?.lang}
            >
              <PlayerImageLang src={language.flag} />
              {language.name}
            </PlayerLang>
          );
        })}
      </PlayerLangs>
      <PlayerOptions isMovie>
        {loading && <Loading />}
        {options
          .filter((o) => o.lang === lang)
          .map((option, idx) => {
            const servidor = servers.find(
              (it) => it.code_flix == option.server
            );
            const language = languages.find(
              (it) => it.code_flix == option.lang
            );
            return (
              <PlayerOption key={idx}>
                <PlayerOptionLink
                  active={option.index === server?.index}
                  onClick={() => changeServer(option)}
                >
                  <PlayerImage src={language?.flag} />
                  <PlayerOptionSubTitle>opción {idx + 1}</PlayerOptionSubTitle>
                  <PlayerOptionText>
                    {servidor?.name} | {langsName[option.lang]}
                  </PlayerOptionText>
                </PlayerOptionLink>
              </PlayerOption>
            );
          })}
      </PlayerOptions>
      {avalibleReports && (
        <PlayerOptionReport onClick={() => changeVisible(true)}>
          <PlayerOptionTitle>
            <AiFillAlert size={20} />
            <PlayerOptionSubTitle>Reportar</PlayerOptionSubTitle>
          </PlayerOptionTitle>
        </PlayerOptionReport>
      )}
    </PlayerOptionsCont>
  );
}
