import { useState } from "react";

import { PlayerModal } from "./Modal";
import { Video, VideoFrame, PlayerVideo, PlayerIFrame, PlayerVideoOptions } from "./styles";
import { PlayerOptionsPicker } from "./options";

export function PlayerMovie({ links, movie }) {
  const [optionActual, changeOption] = useState();
  const [visible, changeVisible] = useState(false);

  return (
    <PlayerVideoOptions>
      <PlayerVideo>
        <Video movie>
          <VideoFrame>
            <PlayerIFrame src={optionActual?.link} allowFullScreen frameborder="0" />
          </VideoFrame>
        </Video>
      </PlayerVideo>
      <PlayerOptionsPicker
        links_online={links}
        server={optionActual}
        changeServer={changeOption}
        _id={movie._id}
        changeVisible={changeVisible}
      />
      <PlayerModal visible={visible} changeVisible={changeVisible} movie={movie} option={optionActual} />
    </PlayerVideoOptions>
  );
}
