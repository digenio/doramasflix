import { Spinner } from 'styles/base'
import { LoadingContainer } from './styles'

export function Loading ({ size = 25 }) {
  return (
    <LoadingContainer>
      <Spinner size={size} /> Cargando...
    </LoadingContainer>
  )
}
