import { MetaText, MetaTitle, MetaStrong, MetaContainer } from './styles'

export function MetaEpisode ({ name, number, season }) {
  return (
    <MetaContainer>
      <MetaTitle>{`Ver ${name} episodio ${season}X${number}`}</MetaTitle>
      <MetaText>
        <MetaStrong>{`Ver ${name} Episodio ${number} Online `}</MetaStrong>
        {` Gratis en HD con audio Latino y Subtitulado.`}
      </MetaText>
      <MetaText>
        ¡Bienvenido a Doramasflix! Esperamos que la estés pasando genial viendo
        <MetaStrong>{` ${name} Episodio ${number} sub español`}</MetaStrong>. Si
        tienes algún problema con el video de
        <MetaStrong>{` ${name} Ep ${number}`}</MetaStrong>, no dudes en usar el
        botón de "Reportar video". Recuerda que ver
        <MetaStrong>{` ${name} ${season}x${number} online `}</MetaStrong>
        nunca ha sido tan fácil en nuestra página web. En Doramasflix te
        proveemos los mejores estrenos de doramas online completas en alta
        calidad 720p y 1080p. No olvides de compartir el dorama
        <MetaStrong> {name}</MetaStrong> con tus amigos y dejarnos un
        comentario.
      </MetaText>
    </MetaContainer>
  )
}
