import { MetaText, MetaStrong, MetaContainer } from './styles'

export function MetaCast ({ name, gender, type }) {
  return (
    <MetaContainer>
      <MetaText>
        <MetaStrong>{`Ver ${type} de ${name} Online`}</MetaStrong> en 720p HD y
        1080p Full HD. Recuerda que en Doramasflix, puedes disfrutar de los{' '}
        <MetaStrong>
          {`${type} ${gender === 1 ? 'de la actriz' : 'del actor'} ${name}`}
        </MetaStrong>{' '}
        en Sub Español, Latino en HD Gratis y Completas.
      </MetaText>
    </MetaContainer>
  )
}
