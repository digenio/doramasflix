import Link from 'next/link'

import { Figure, Image } from './styles'

export function Logo () {
  return (
    <Link href='/'>
      <a>
        <Image width='140' height='40' src='/img/logo.png' />
      </a>
    </Link>
  )
}
