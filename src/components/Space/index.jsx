import { SpaceContainer } from './styles'

export function Space ({ width, height }) {
  return <SpaceContainer width={width} height={height} />
}
