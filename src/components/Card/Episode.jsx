import Img from 'react-cool-img'
import Link from 'next/link'
import { MdPlayArrow } from 'react-icons/md'

import { colors } from 'styles/theme'
import {
  CardLink,
  IconPlay,
  CardYear,
  CardLabel,
  CardHover,
  CardTitle,
  CardLabels,
  CardFigure,
  CardContainer,
  CardExtraLabels,
  CardContainerImage
} from './styles'

export function CardEpisode ({
  as,
  image,
  label,
  title,
  width,
  state,
  height,
  srcSet,
  children,
  href = '/'
}) {
  return (
    <CardContainer width={width} height={height}>
      <Link href={href} as={as || href}>
        <a>
          <CardContainerImage>
            <IconPlay>
              <MdPlayArrow size={25} color={colors.primary} />
            </IconPlay>
            {image ? (
              <CardFigure>
                <Img
                  placeholder='/img/notcover.jpeg'
                  error='/img/notcover.jpeg'
                  style={{
                    backgroundColor: colors.backg,
                    width: '100%',
                    minHeight: '20px',
                    maxHeight: 150
                  }}
                  src={image}
                  srcSet={srcSet && srcSet}
                />
              </CardFigure>
            ) : (
              <CardFigure>
                <Img
                  style={{
                    backgroundColor: colors.backg,
                    width: '100%',
                    minHeight: '20px',
                    maxHeight: 150
                  }}
                  src='/img/notcover.jpeg'
                  //srcSet={srcSet && srcSet}
                />
              </CardFigure>
            )}
            <CardLabels>{label && <CardLabel>{label}</CardLabel>}</CardLabels>
            <CardExtraLabels>
              {state && <CardYear>{state}</CardYear>}
            </CardExtraLabels>
          </CardContainerImage>
        </a>
      </Link>
      {title && <CardTitle>{title}</CardTitle>}
      {children && <CardHover>{children}</CardHover>}
    </CardContainer>
  )
}
