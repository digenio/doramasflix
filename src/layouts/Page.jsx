import { Main } from "styles/global";
import { Header } from "components/Header";
import { Footer } from "../components/Footer";
import { ContextsProviders } from "contexts/Providers";
import SmartBanner from "react-smartbanner";

export function Page({ children }) {
  return (
    <ContextsProviders>
      <Header />
      <Main>{children}</Main>
      <Footer />
      <SmartBanner
        title={"DoramasGO!"}
        button="Descargar"
        storeText={{ android: "En Google Play" }}
        price={{ android: "Gratis" }}
        position="bottom"
        url={{
          android:
            "https://play.google.com/store/apps/details?id=com.asiapp.doramasgo",
        }}
        daysHidden={3}
        daysReminder={10}
      />
    </ContextsProviders>
  );
}
